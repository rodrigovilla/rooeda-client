import React from 'react';
import './Calendar.scss?v=2.0.0';
import { Link } from 'react-router-dom';
import { Form, Tab, Tabs, Dropdown, TextInput, Button, Slider } from 'carbon-components-react';
import 'react-responsive-modal/styles.css';
import { Modal } from 'react-responsive-modal';
import { Checkmark32, TrashCan32, UserFollow32 } from '@carbon/icons-react';
import axios from 'axios';
import * as dateFns from "date-fns";
import appSettings from '../../helpers/AppSettings';
import dateTranslator from '../../helpers/DateTranslator';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

class Calendar extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      data: [],
      isLoading: true,
      errors: null,
      open: false,
      time: '', instructor: '', music: '',
      selectedItem: '',
      currentDate: '',
      currentTime: '',
      instructors: [],
      modifiedTime: 0,
      classRoomsAvailable: [],
      selectedClassRoom: '',
      WeekPlaced: 0,
      openModalConfirmation: false,
      openModalConfirmationDropDown: false,
      lastValue: '',
      lastWeek: '',
      classRoomId: ''
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.saveSchedule = this.saveSchedule.bind(this);
    this.currentWeek = this.currentWeek.bind(this);
    this.nextWeek = this.nextWeek.bind(this);
    this.deleteBox = this.deleteBox.bind(this);
  }

  handleChange(event) {
    let nam = event.target.name;
    let val = event.target.value;
    this.setState({[nam]: val});
  }

  onOpenModal = () => {
    this.setState({ open: true });
  };

  onCloseModal = () => {
    this.setState({ selectedItem : '', music: '', instructor: '', open: false, name: '', modifiedTime: 0, openModalConfirmation: false, openModalConfirmationDropDown: false })
  };

  getPosts() {
    axios.all([
      axios.get(`/api/class-room/all`),
      axios.get(`/api/instructor/all`)
    ]).then(axios.spread((classRooms, instructors) => {
      let classRoomId = (classRooms.data.length > 0) ? classRooms.data[0]._id : null;
      axios.get(`${appSettings.SERVER_URL}/schedule/admin/${this.state.WeekPlaced}/${classRoomId}`).then( schedules => {
        this.setState({
          data: schedules.data,
          isLoading: false,
          instructors: instructors.data,
          classRoomsAvailable: classRooms.data,
          selectedClassRoom: (classRooms.data.length > 0) ? classRooms.data[0].Name : null,
          classRoomId: classRoomId
        });
      });
    })).catch(error => this.setState({ error, isLoading: false }));
  }

  componentDidMount() {
    this.getPosts();
  }

  getMonday() {
    let d = new Date();
    var day = d.getDay(),
        diff = d.getDate() - day + (day === 0 ? -6:1); // adjust when day is sunday
    return new Date(d.setDate(diff));
  }

  getHours(){
    const init = new Date('Jul 27, 2011 06:00:00 AM');
    const last = new Date('Jul 27, 2011 09:00:00 PM');
    var hoursOfDay = [];

    for (var hour = init.getHours(); parseInt(hour) <= parseInt(last.getHours()); hour++) {
      let t = new Date('Jul 27, 2011 ' +  hour + ":00 ")
      hoursOfDay.push({Hour :dateFns.format(t, "h") + ":00 " + dateFns.format(t, "a")});
    }

    return hoursOfDay;
  }


  handleSubmit(e){
    e.preventDefault();
    let values =  { Time: this.state.time, Music: this.state.music, Instructor: this.state.instructor, CurrentDate: this.state.currentDate, CurrentTime: this.state.currentTime, Minutes: this.state.modifiedTime};
    let week = this.state.data;

    let hour = values.CurrentTime.split(':')[0];
    let min = values.CurrentTime.split(':')[1].split(' ')[0];
    let am_pm = values.CurrentTime.split(':')[1].split(' ')[1]

    let minutes = (values.Minutes > 0) 
                ? (values.Minutes <= 9) ?  `0${values.Minutes}` : values.Minutes.toString()
                : min;

    let newTime = `${hour}:${minutes} ${am_pm}`;

    week.forEach(x => {
      if(x.Date === values.CurrentDate){
        var json = {
          Instructor: values.Instructor,
          Music: values.Music,
          Time: values.CurrentTime,
          ModifiedTime: newTime
        }
        
        x.Hours = x.Hours.filter(y => json.Time !== y.Time);
        x.Hours = [...x.Hours, json];
        x.Hours.sort((a, b) => new Date('Jul 27, 2011 ' + a.Time) - new Date('Jul 27, 2011 ' + b.Time));
      }
    });

    this.setState({data: week})
    this.onCloseModal();
  }


  saveSchedule(e){
    e.preventDefault();
    let json = {
      data: this.state.data,
      classRoomId: this.state.classRoomId
    };

    axios.post(`${appSettings.SERVER_URL}/schedule/add`, json)
    .then(function (response) {
        //--- Toast notification
        toast("Calendario guardado");
    })
    .catch(function (error) {
        //--- Toast de error
        toast("Error!");
    });
  }

  nextWeek() {
    let currentComponent = this;
    axios.get(`${appSettings.SERVER_URL}/schedule/admin/${this.state.WeekPlaced}/${this.state.classRoomId}`)
    .then(function (response) {
      currentComponent.setState({ data: response.data , openModalConfirmation: false});
    }).catch();
  }

  currentWeek() {
    this.onCloseModal();
    let currentComponent = this;
    axios.get(`${appSettings.SERVER_URL}/schedule/admin/${this.state.WeekPlaced}/${this.state.classRoomId}`)
    .then(function (response) {
      currentComponent.setState({ data: response.data });
    }).catch();
  }

  deleteBox(){
    var{ currentDate, currentTime, data} = this.state; 
    
    data.forEach(x => {
      if(x.Date === currentDate){
        x.Hours= x.Hours.filter(y => y.Time !== currentTime);
      }
    });

    this.setState({ data: data, instructor : '', music : '', time : '', open : false, modifiedTime: 0});
  }

  setClassRoom(classRoomNumber){
    let classRoom = this.state.classRoomsAvailable.find(x => x.Name === classRoomNumber);
    let currentClass = this.state.selectedClassRoom;
    let currentWeek = this.state.WeekPlaced;
    this.setState({ classRoomId: classRoom._id , selectedClassRoom: classRoomNumber, WeekPlaced: 0, openModalConfirmationDropDown: true, lastValue: currentClass, lastWeek: currentWeek});
  }

  week(place){
    this.setState({ openModalConfirmation: true, WeekPlaced: place })
  }

  convertMinutesInt(time){    
    return +time.split(':')[1].split(' ')[0];
  }

  render() {
    const { isLoading, data } = this.state;
    const { open } = this.state;
    const schedule = this.getHours();
    let month = '';
    
    if(data.length > 0){
      var start = dateFns.format(new Date(data[0].Date), 'MMMM')
      var end = dateFns.format(new Date(data[data.length - 1].Date), 'MMMM')
      month = (start !== end)
        ? `${dateTranslator.monthTraslator(start)} - ${dateTranslator.monthTraslator(end)}`
        : `${dateTranslator.monthTraslator(start)}`
    }

    return (
      <React.Fragment>
         <Modal 
            open={open} 
            onClose={this.onCloseModal} 
            center
            animationDuration={80}
            modalId={3} 
            >
         <div>
         <Form onSubmit={this.handleSubmit}>
              <span className="bx--label">Instructor</span>
              <Dropdown items={this.state.instructors.map(x => x.Name)} value={this.state.instructor} label="" name="instructor" id='instructor' onChange={({ selectedItem }) => this.setState({ instructor: selectedItem })}
              selectedItem={this.state.instructor}/>
              <br></br>
              <span className="bx--label">Música</span>
              <TextInput name="music" value={this.state.music} onChange={this.handleChange}  className= 'music' id='music'></TextInput>
              <br></br>
              <span className="bx--label">Minutos adicionales</span>
              <Slider
                name="minutes"
                max={55}
                min={0}
                value={this.state.modifiedTime}
                step={5}
                inputType="number"
                onChange={($event) => this.setState({ modifiedTime: $event.value })}
               ></Slider>
              <br></br>
              <Button 
                renderIcon={Checkmark32}  
                type="submit" 
                value="Submit" 
                size='small'
                kind='ghost' 
              >
                Confirmar
              </Button>
              {(this.state.instructor) ? 
              <Button 
                renderIcon={TrashCan32}  
                type="submit" 
                onClick={this.deleteBox} 
                kind='ghost' 
                size='small'
              >
                Borrar
              </Button>
              : null }
              </Form>
            </div>
        </Modal>

        {(!this.state.classRoomsAvailable && this.state.classRoomsAvailable.length === 0) ?
          <div className="bx--grid bx--grid--full-width calendarContainer">
          <div className="bx--row">
          <div className="bx--col-md-4">
            <h2 className="no-class-rooms">
              No hay salónes
            </h2> 
            <div class='no-class-rooms-button'>
            <Link element={Link} to="/add-class-room">
                <Button size="small" kind="secondary" renderIcon={UserFollow32}>Agregar salón</Button>
              </Link>
            </div>
          </div>
          </div>
          </div>
        :

        <div className="bx--grid bx--grid--full-width calendarContainer">
        <div className="calendarControlArea">

          <div className="bx--row">

            <div className="bx--col-xlg-16 bx--col-lg-6 bx--col-md-6 bx--col-sm-16">
              <h1 className="calendarTitle">{month}</h1>
            </div>

            <div className="bx--col-xlg-16 bx--col-lg-6 bx--col-md-6 bx--col-sm-16">
            <div className="align-right">
              <Button renderIcon={Checkmark32} onClick={this.saveSchedule} kind="secondary" size="small">Guardar</Button>
            </div>

            {(this.state.classRoomsAvailable.length > 1) ?
              <div className="align-right">
                  <Dropdown 
                    items={this.state.classRoomsAvailable.map(x => x.Name)} 
                    value={this.state.selectedClassRoom} 
                    label=""  
                    name="class-room" id='instructor' 
                    onChange={({ selectedItem }) => this.setClassRoom(selectedItem)}
                    selectedItem={this.state.selectedClassRoom} type="inline"
                    className="marginFix"
                  />
              </div>
                : null }
              </div>
          </div>

        
        <div className="calendarTitle" style={{ width: '90%'}}>
        <Tabs selected={this.state.WeekPlaced}>
          <Tab
            name="Semana-actual"
            href="#"
            id="tab-1"
            label="Semana actual"
            onClick={() => this.week(0)}
          >
            <div className="some-content">
            </div>
          </Tab>
          <Tab
            name="Semana-proxima"
            href="#"
            id="tab-2"
            label="Semana próxima"
            onClick={() => this.week(1)}
          >
            <div className="some-content">
            </div>
          </Tab>
        </Tabs>
        </div>
        
        {/* Contro Area ends  */}
        </div>

        <div className="calendarScrolling">
        <div className="bx--row">

        {/* Time column  */}
          <div className="bx--col-calendar bx--col-md-1">
          <div className="calendarDay"></div>

          { schedule.map( (hour, index) => {
                return(
                  <div key={index} className="bx--aspect-ratio bx--aspect-ratio--1x1 calendarTime">
                  <div className="bx--aspect-ratio--object">
                      <div className="calendarTime">
                      <h5>{hour.Hour}</h5>
                      </div>
                  </div>  
                 </div> 
                );
              })
          }
        </div>
        

        {/* Row starts  */}
        {!isLoading ? (
            data.map( (week, weekIndex) => {
              var h = schedule.map( (time, index) => {
                //var t = week.Hours.find(x => x.Time === time.Hour);
                var t = week.Hours.find(x => x.Time === time.Hour);
                if(t){
                  return(
                    <div key={index} className="bx--aspect-ratio bx--aspect-ratio--1x1 Box">
                      <div className="bx--aspect-ratio--object">
                      <div className="BoxContent" onClick={() => this.setState({ currentDate: week.Date, currentTime: time.Hour , open: true, name:t.Instructor, music: t.Music, instructor: t.Instructor, modifiedTime:  this.convertMinutesInt(t.ModifiedTime)})}>
                        <div >
                          <h6>{t.ModifiedTime}</h6>
                          <p>{t.Instructor.split(' ')[0]}</p>
                          <div className="BoxContentBottom">
                          <p>{t.Music}</p>
                          </div>
                        </div>
                      </div>
                      </div>
                  </div>
                  );
                }else{
                  return(
                    <div key={index} className="bx--aspect-ratio bx--aspect-ratio--1x1 Box">
                      <div className="bx--aspect-ratio--object">
                      <div className="BoxContent" onClick={() => this.setState({ currentDate: week.Date, currentTime: time.Hour , open: true})}>
                        <div >
                          </div>
                        </div>
                      </div>
                      </div>
                  );
                }
              });

              const day = new Date(week.Date);
              return (
                <div key={weekIndex} className="bx--col-calendar bx--col-md-1">
                <div className="calendarDay">{week.Day} <span>{dateFns.format(day, "d")}</span></div>
                {h}
                </div>
                 
              );
            })
          ) : (
            <p>Cargando...</p>
          )}

          
      {/* Row starts  */}
      </div>
      </div>
          
      </div>
  }

      <Modal 
            open={this.state.openModalConfirmation} 
            onClose={this.onCloseModal} 
            center
            animationDuration={80}
            modalId={3} 
            >
         <div>
                Revisa si ya guardaste tus cambios, si presionas continuar no se guardaran tus cambios
              <br></br> <br></br>
              <Button 
                renderIcon={Checkmark32}  
                type="submit" 
                value="Submit" 
                size='small'
                kind='ghost' 
                onClick={() => this.setState({ openModalConfirmation: false, WeekPlaced: this.state.WeekPlaced === 0 ? 1 : 0 })}
              >
                Regresar
              </Button>
              <Button 
                renderIcon={Checkmark32}  
                type="submit" 
                value="Submit" 
                size='small'
                kind='ghost' 
                onClick={this.nextWeek}
              >
                Continuar
              </Button>
            </div>
        </Modal>

        <Modal 
            open={this.state.openModalConfirmationDropDown} 
            onClose={this.onCloseModal} 
            center
            animationDuration={80}
            modalId={3} 
            >
         <div>
               Seguro que deseas cambiar de Salón?
              <br></br> <br></br>
              <Button 
                renderIcon={Checkmark32}  
                type="submit" 
                value="Submit" 
                size='small'
                kind='ghost' 
                onClick={() => this.setState({ openModalConfirmationDropDown: false, WeekPlaced: this.state.lastWeek, selectedClassRoom: this.state.lastValue})}
              >
                Regresar
              </Button>
              <Button 
                renderIcon={Checkmark32}  
                type="submit" 
                value="Submit" 
                size='small'
                kind='ghost' 
                onClick={this.currentWeek}
              >
                Continuar
              </Button>
            </div>
        </Modal>

      </React.Fragment>
    );
  }
  }
  export default Calendar;
  
