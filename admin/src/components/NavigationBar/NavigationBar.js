import React from 'react';
import { Link } from 'react-router-dom';
import {
  Header,
  HeaderMenuButton,
  HeaderName,
  HeaderNavigation,
  HeaderMenuItem,
  HeaderGlobalBar,
  HeaderGlobalAction,
  SkipToContent
} from 'carbon-components-react/lib/components/UIShell';
import AppSwitcher20 from '@carbon/icons-react/lib/app-switcher/20';
import Logo from './logo.svg';

const RooedaHeader = () => (
  <>
  <Header aria-label="Rooeda Studio">
    <SkipToContent />
    <HeaderMenuButton aria-label="Open menu burger">
    </HeaderMenuButton>
    <img src={Logo} alt='website logo' className="logo" />
    <HeaderName element={Link} to="/" prefix="Rooeda Studio">
    </HeaderName>
    <HeaderNavigation aria-label="Rooeda Studio">
      <HeaderMenuItem element={Link} to="/instructors">
        Instructores
      </HeaderMenuItem>
      <HeaderMenuItem element={Link} to="/calendar">
        Calendario
      </HeaderMenuItem>
    </HeaderNavigation>

    <HeaderGlobalBar>
      <HeaderGlobalAction aria-label="App Switcher">
        <AppSwitcher20 />
      </HeaderGlobalAction>
    </HeaderGlobalBar>
  </Header>
  </>
);
export default RooedaHeader;
