import React from 'react';
import './CalendarClass.scss';
import { Tab, Tabs, Dropdown } from 'carbon-components-react';
import * as dateFns from "date-fns";
import axios from 'axios';
import ls from 'local-storage'
import appSettings from '../../helpers/AppSettings';
import dateTranslator from '../../helpers/DateTranslator';

class CalendarClass extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      isLoading: true,
      errors: null,
      time: '', 
      instructor: '', 
      music: '',
      classRoomsAvailable: [],
      selectedClassRoom: '',
      WeekPlaced: 0 ,
      classRoomId: ''
    };

    this.currentWeek = this.currentWeek.bind(this);
    this.nextWeek = this.nextWeek.bind(this);
  }

  getPosts() {
    axios.all([
      axios.get(`/api/class-room/all`)
    ]).then(axios.spread((classRooms) => {
      let classRoomId = (classRooms.data.length > 0) ? classRooms.data[0]._id : null;
      axios.get(`${appSettings.SERVER_URL}/schedule/admin/0/${classRoomId}`).then( schedules => {
        this.setState({
          data: schedules.data,
          isLoading: false,
          classRoomsAvailable: classRooms.data,
          selectedClassRoom: (classRooms.data.length > 0) ? classRooms.data[0].Name : null,
          classRoomId: classRoomId
        });
      });
    })).catch(error => this.setState({ error, isLoading: false }));
  }

  componentDidMount() {
    this.getPosts();
  }

  nextWeek() {
    this.setState({ WeekPlaced: 1 })
    let currentComponent = this;
    axios.get(`${appSettings.SERVER_URL}/schedule/admin/1/${this.state.classRoomId}`)
    .then(function (response) {
      currentComponent.setState({ data: response.data });
    }).catch();
  }

  setClassRoom(classRoomNumber){
    let classRoom = this.state.classRoomsAvailable.find(x => x.Name === classRoomNumber);
    this.setState({ selectedClassRoom: classRoomNumber, WeekPlaced: 0,classRoomId: classRoom._id  })
    this.currentWeek(classRoom._id);
  }

  currentWeek(classRoomNumber) {
    let currentComponent = this;
    let result = (classRoomNumber) ? classRoomNumber : this.state.classRoomId;
    axios.get(`${appSettings.SERVER_URL}/schedule/admin/0/${result}`)
    .then(function (response) {
      currentComponent.setState({ data: response.data });
    }).catch();
  }

  getSchedule(content){
    ls.set("classRoomContent", content);
    window.location.href=`/class-content`;
  }

  render() {
    const { isLoading, data } = this.state;
    let month = '';

    if(data.length > 0){
      var start = dateFns.format(new Date(data[0].Date), 'MMMM')
      var end = dateFns.format(new Date(data[data.length - 1].Date), 'MMMM')
      month = (start !== end)
        ? `${dateTranslator.monthTraslator(start)} - ${dateTranslator.monthTraslator(end)}`
        : `${dateTranslator.monthTraslator(start)}`
    }

    return (
      <div className="bx--grid bx--grid--full-width calendarContainer">
        <div className="calendarControlArea">

          <div className="bx--row">
            <div className="bx--col-sm-16 bx--col-md-16 bx--col-lg-8 bx--col-xlg-8">
              <h1 className="calendarTitle">{month}</h1>
            </div>
            
            <div className="bx--col-md-16  bx--col-sm-16 bx--col-lg-4 bx--col-xlg-4">
            {(this.state.classRoomsAvailable.length > 1) ?
            <div className="align-right">
                <Dropdown items={this.state.classRoomsAvailable.map(x => x.Name)} value={this.state.selectedClassRoom} label=""  name="class-room" id='instructor' onChange={({ selectedItem }) => this.setClassRoom(selectedItem)}
                selectedItem={this.state.selectedClassRoom} type="inline"/>
            </div>
              
                : null }
            </div>

            <div className="bx--col"></div>
            <div className="bx--col">
            </div>
          </div>

        
        <div style={{ width: '90%'}}>
        <Tabs selected={this.state.WeekPlaced}>
          <Tab
            name="Semana-actual"
            href="#"
            id="tab-1"
            label="Semana actual"
            onClick={() => this.currentWeek()}
          >
            <div className="some-content">
            </div>
          </Tab>
          <Tab
            name="Semana-proxima"
            href="#"
            id="tab-2"
            label="Semana próxima"
            onClick={() => this.nextWeek()}
          >
            <div className="some-content">
            </div>
          </Tab>
        </Tabs>
        
        </div>
        
        </div>
      <div className="calendarScrolling">
      <div className="bx--row">
          
      {!isLoading ? (
            data.map( (week, i) => {
              const day = new Date(week.Date);
                var h = week.Hours.map((t, index) => {
                  return (
                    <div key={index}>
                        <div className="bx--aspect-ratio bx--aspect-ratio--1x1 Box">
                          <div className="bx--aspect-ratio--object">
                          <div className="BoxContent" onClick={() => this.getSchedule({ id: week.ClassRoomId, time: t.Time, modifiedTime: t.ModifiedTime, day: week.Day, number: dateFns.format(day, "d"), date: week.Date})} >
                            <div >
                              <h6>{t.ModifiedTime}</h6>
                              <p>{t.Instructor.split(' ')[0]}</p>
                              <div className="BoxContentBottom">
                              <p>{t.Music}</p>
                              </div>
                            </div>
                          </div>
                         </div>
                         </div>
                    </div>
                  );
                });
                
              return (
                <div key={i} className="bx--col-calendar bx--col-md-1">
                <div className="calendarDay">{week.Day} <span>{dateFns.format(day, "d")}</span></div>
                {h}
                </div>
                 
              );
            })
          ) : (
            <p>Loading...</p>
          )}
      </div>
      </div>
      </div>

    );
  } 
}
export default CalendarClass;
