import React from "react";
import './Sidebar.scss?v=2.0.0';
import LogoRooeda from './logo-rooeda.svg';
import Sidebar from "react-sidebar";
import { Link } from 'react-router-dom';
import ls from 'local-storage'



class SidebarRooeda extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      sidebarOpen: false,
      session: ls.get('session')
    };
    this.onSetSidebarOpen = this.onSetSidebarOpen.bind(this);
  }

  onSetSidebarOpen(open) {
    document.body.style.overflow =  (open) ? "hidden" : "";
    this.setState({ sidebarOpen: open });
  }
  

  render() {
    const renderAuthButton = ()=>{
      if(this.state.session !== null){
        return <Link element={Link} to="/logout">Cerrar sesión</Link> 
      }
    }

    if(this.state.session){
      return (
        <Sidebar
          sidebar={
              <div className="SidebarContainer">
              <ul className="Sidebar">
                  <Link element={Link} to="/">
                      <img src={LogoRooeda} alt='Rooeda Studio' className="SidebarLogo" />
                  </Link>
                  <li><Link element={Link} to="/">Inicio</Link></li>
                  <li><Link element={Link} to="/instructors">Instructores</Link></li>
                  <li><Link element={Link} to="/calendar">Calendario</Link></li>
                  <li><Link element={Link} to="/members">Miembros</Link></li>
                  <div className="alignBottom" data-type='sideBar'>
                  <li className="rooedaSidebarLoginControl">
                    {renderAuthButton()}
                  </li>
              </div>
              </ul>

              </div>
          }
          open={this.state.sidebarOpen}
          onSetOpen={this.onSetSidebarOpen}
          shadow={false}
          styles={{ 
              root: { },
              sidebar: { 
                  position: "absolute",
                  background: "rgba(25,25,25, 0.70)", 
                  backdropFilter: "saturate(180%) blur(20px)",
                  WebkitBackdropFilter: "saturate(180%) blur(20px)",
                  paddingTop: "25pt",
                  paddingLeft: "0pt",
                  width: "25wv",
                  zIndex: 99999,
                  transition: "transform .2s ease-out",
                  WebkitTransition: "-webkit-transform .2s ease-out"
              },
              content: { overflowY: 'auto' },
              overlay: {
                  backgroundColor: "rgba(25,25,25, 0.30)",
                  overflow: "hidden",
                  zIndex: 999,
                  transition: "opacity .1s ease-out, visibility .1s ease-out",
              }
          }}
          >
          <div 
              className="burguerMenu" 
              onClick={() => this.onSetSidebarOpen(true)} 
          >
          </div>
        </Sidebar>
      );
    }else{
      return null;
    }
  }
}

export default SidebarRooeda;
