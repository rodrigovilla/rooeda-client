import React from 'react';
import './Footer.scss?v=2.0.0';
import IconLocked from './icon-locked.svg';


const Footer = () => {
    return (

      <>
      <div className="footerStyle">
        <div className="bx--grid bx--grid--full-width landing-page">

            <div className="bx--row footerRooeda">
              <div className="bx--col-md-6 bx--col-lg-6">
                  <img src={IconLocked} alt='Rooeda Studio' className="footerIconLocked" />
                  <p>Protegido con SSL <span>|</span>{new Date().getFullYear()} Rooeda™ Studio</p>
              </div>
              <div className="bx--col-md-6 bx--col-lg-6">
                  <a href="https://dashboard.stripe.com" target="_blank" rel="noopener noreferrer" >
                      Finanzas en Stripe
                  </a>
              </div>
            </div>

        </div>
      </div>
      </>
    );
  };
  export default Footer;