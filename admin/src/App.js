import React, { Component } from 'react';
import './App.scss';
import { Route, Switch } from 'react-router-dom';
import { Content } from 'carbon-components-react/lib/components/UIShell';
import Header from './components/Header';
import Sidebar from './components/Sidebar';
import Calendar from './components/Calendar';
import Home from './content/Home';
import Profile from './content/Profile';
import Signup from './content/Signup';
import Login from './content/Login';
import ForgotPass from './content/ForgotPass';
import NewPass from './content/NewPass';
import Instructors from './content/Instructors';
import Members from './content/Members';
import Reservations from './content/Reservations';
import AddInstructor from './content/AddInstructor';
import Logout from './content/Logout';
import ClassRoom from './content/ClassRoom';
import AddClassRoom from './content/AddClassRoom';
import CalendarClass from './components/CalendarClass';
import ClassRoomContent from './content/ClassRoomContent';
import AddPack from './content/AddPack';
import Packs from './content/Packs';
import PauseControl from './content/PauseControl';
import Settings from './content/Settings';
import Footer from './components/Footer';
import CacheBuster from './CacheBuster';
import { ToastContainer } from 'react-toastify';

class App extends Component {
  render() {
    return (
      <>
        <ToastContainer 
            position="top-right"
            autoClose={3000}
        >
        </ToastContainer>

        <Content className="content">
        <Sidebar></Sidebar>
        <Header></Header>
        <CacheBuster>
        {({ loading, isLatestVersion, refreshCacheAndReload }) => {
          if (loading) return null;
          if (!loading && !isLatestVersion) {
            refreshCacheAndReload();
          }
          
          return null;
        }}
      </CacheBuster>
          <Switch>
            <Route exact path="/" component={Home} />
            <Route path="/profile" component={Profile} />
            <Route path="/signup" component={Signup} />
            <Route path="/login" component={Login} />
            <Route path="/logout" component={Logout} />
            <Route path="/forgotpass" component={ForgotPass} />
            <Route path="/newpass" component={NewPass} />
            <Route path="/instructors" component={Instructors} />
            <Route path="/addinstructor" component={AddInstructor} />
            <Route path="/calendar" component={Calendar} />
            <Route path="/members" component={Members} />
            <Route path="/reservations" component={Reservations} />
            <Route path="/class-room" component={ClassRoom} />
            <Route path="/add-class-room" component={AddClassRoom} />
            <Route path="/calendar-class" component={CalendarClass} />
            <Route path="/class-content" component={ClassRoomContent} />
            <Route path="/add-pack" component={AddPack} />
            <Route path="/select-pack" component={Packs} />
            <Route path="/pause-control" component={PauseControl} />
            <Route path="/settings" component={Settings} />
          </Switch>
          <Footer></Footer>
        </Content>
      </>
    );
  }
}

export default App;
