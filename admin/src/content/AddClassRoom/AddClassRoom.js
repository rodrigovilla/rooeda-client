import React from 'react';
import '../../components/LoginControl/LoginControl.scss?v=2.0.0';
import './AddClassRoom.scss';
import { Form, TextInput, Button } from 'carbon-components-react';
import axios from 'axios';
import appSettings from '../../helpers/AppSettings';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';


const NameInputProps = {
  className: 'name',
  id: 'name',
  labelText: 'Nombre',
  placeholder: ''
};
const EmailInputProps = {
  className: 'age',
  id: 'email',
  labelText: 'No. Bicicletas',
  placeholder: ''
};


class AddClassRoomForm extends React.Component {

  constructor(props) {
    super(props);
    

    let t = props.location.pathname.split('/');
    var id = '';
    if(t[2]){
      id = t.pop();
    }

    this.state = {name: '', noBikes: 0, classRoomId: id, disableButton: false};

    this.handleChange = this.handleChange.bind(this);
  }

  componentDidMount() {
    if(this.state.classRoomId){
      axios.get(`${appSettings.SERVER_URL}/class-room/id/${this.state.classRoomId}`)
      .then(classRoom =>{
        let { Name, NumberOfBikes } = classRoom.data;
        this.setState({ name: Name, noBikes: NumberOfBikes })
      }  )
      .catch(error => this.setState({ error, isLoading: false }));
    }
  }

  onClickHandler = (e) => {
    e.preventDefault();
    let values =  { Name: this.state.name, NumberOfBikes: this.state.noBikes } 

    if(!values.Name || !values.NumberOfBikes){
      toast("Llena los campos requeridos");
      return
    }

    this.setState({disableButton: true});
    axios.put(`${appSettings.SERVER_URL}/class-room/${this.state.classRoomId}`, values)
    .then(() => {
      let msg = (this.state.classRoomId) ? 'Salón modificado' : 'Salón agregado' ;
      toast(msg);
      setTimeout(() => {
        window.location.href = '/class-room'
      }, 4000);
    }).catch();
  }

  handleChange(event) {
    let nam = event.target.name;
    let val = event.target.value;
    let cero = (event.target.name === 'noBikes' && event.target.value < 0);
    let exceeded = (event.target.name === 'noBikes' && event.target.value > appSettings.MAX_NUMBER_OF_BIKES);
    
    if(cero){
      val = 0;
    }

    if(exceeded){
      val = appSettings.MAX_NUMBER_OF_BIKES;
      toast(`La capacidad maxima de bicicletas es de ${appSettings.MAX_NUMBER_OF_BIKES}`);
    }

    this.setState({[nam]: val});
  }

  render() {
    return (
      <div className="dashBoard">
      <div className="formStyle" data-type='AddInstructor'>
      <div className="bx--grid">
      <div className="bx--row">
        <div className="bx--col-lg-4 bx--col-md-2 bx--col-sm-1"></div>
        <div className="bx--col-lg-4 bx--col-md-4 bx--col-sm-4">
          <Form>

              <h3 className="formTitles fileUploaderLabel">
                {(!this.state.classRoomId) 
                ? 'Agregar'
                : 'Modificar'} salón
              </h3>

              <TextInput
              name='name'
              type="text" 
              value={this.state.name} 
              onChange={this.handleChange} 
              required
              {...NameInputProps}  
              />


              <TextInput
              name='noBikes'
              type="number" 
              min="0"
              max={appSettings.MAX_NUMBER_OF_BIKES}
              value={this.state.noBikes} 
              onChange={this.handleChange} 
              required
              {...EmailInputProps}  
              />

              <br></br>
              <Button 
                  type="submit" 
                  value="Submit" 
                  className="buttonAccess"
                  size="small"
                  kind="secondary"
                  disabled={this.state.disableButton}
                  onClick={this.onClickHandler}
              >
                  Guardar
              </Button>

          </Form>

        </div>  
        <div className="bx--col-lg-4 bx--col-md-2 bx--col-sm-1"></div>
      </div>
      </div>
      </div>
      </div>
    );
  }
}

export default AddClassRoomForm;