import React from 'react';
import { Link } from 'react-router-dom';
import { Undo32} from '@carbon/icons-react';
import { Button } from 'carbon-components-react';
import './Reservations.scss?v=2.0.0';
import axios from 'axios';
import appSettings from '../../helpers/AppSettings';
import ls from 'local-storage'

class Reservations extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      clases: 0,
      expire: '',
      data: [],
      classRooms: [],
      id: props.location.pathname.split('/').pop()
    };
  }

  componentDidMount() {
    axios.all([
      axios.get(`${appSettings.SERVER_URL}/user-pack/available/${this.state.id}`),
      axios.get(`${appSettings.SERVER_URL}/user-schedule/id/${this.state.id}`),
      axios.get(`${appSettings.SERVER_URL}/class-room/all`)
    ]).then(axios.spread((pack, schedule, classRooms) => {
      if(pack.status === 200){
        var expire;
        var totalClases = 0;
        var index = 0;
        
        if(pack.data.length > 0){
          expire = pack.data[0].Expires;
          pack.data.forEach( (element, i) => {
            totalClases += element.Clases;
            if(new Date(expire) < new Date(element.Expires)){
              expire = element.Expires;
              index =  i;
            }
          });

          var y = this.datediff(expire);
          totalClases = (y > 0) ? totalClases: 0;
          this.setState({clases: totalClases, expire: expire});
        }
      }

        if(schedule.status === 200){
          this.setState({data: schedule.data});
        }

        if(classRooms.status === 200){
          this.setState({classRooms: classRooms.data});
        }
        this.setState({name: ls.get('memberName')})
      })).catch(error => this.setState({ error, isLoading: false }));
  }

  setClassRoomName(classRoomId){
    let result =  this.state.classRooms.find(x => x._id === classRoomId);
    return (result) ? result.Name: null;
  }

  datediff(date) {
    var now = new Date();
    //var today = Date.UTC(now.getFullYear(), now.getUTCMonth(), now.getDate())
    var d = date ? date : this.state.expire;
    var expire = new Date(d);
    var value = Math.round((expire-now)/(1000*60*60*24));
    //var value = Math.round((expire-today)/(1000*60*60*24));
    return (value > 0) ? (value - 1) : 0;
  }

  render(){
    return (
      <div className="dashBoard">
      <div className="formStyle">
      <div className="bx--row">
                  <div className="bx--col-md-2"></div>
                  <div className="bx--col-md-4">
                      <div className="centerTitle">
                          <h3>{this.state.name}</h3>
                      </div>
                      <div className="profileContainer">
                        <div className="bx--aspect-ratio bx--aspect-ratio--1x1 packBox" data-type='profile'>
                          <div className="bx--aspect-ratio--object centerTitle">
                            <div className="packContent">
                              <h2>{this.state.clases}</h2>
                              <h4>clases</h4>
                            <div className="packContentBottom" data-type='expiration'>
                              {(this.state.expire) ?
                              <p>Expira en <span>{this.datediff()}</span> días</p>
                              : null }
                            </div>
                            </div>
                          </div>
                        </div>
                       
                      </div>
                  </div>
                  <div className="bx--col-md-2"></div>
                  </div>

      {(this.state.data.length > 0)
                    ?
                    <>
                    <div className="bx--row">
                      <div className="bx--col-lg-4"></div>
                      <div className="bx--col-lg-4 centerTitle">
                        <h1>Reservaciones</h1>
                      </div>
                      <div className="bx--col-lg-4"></div>
                    </div>

                    {/* <div className="bx--row">
                        <div className="bx--col-md-2"></div>
                        <div className="bx--col-md-4">
                          <h3> {this.state.name}</h3>
                        </div>
                      <div className="bx--col-md-2"></div>
                    </div> */}
                    <br></br>

                    <div className="bx--grid">
                      <div className="bx--row">
                        <div className="bx--col-sm-1 bx--col-md-2 bx--col-lg-3 bx--col-xlg-3"></div>
                          <div className="bx--col-sm-4 bx--col-md-4 bx--col-lg-6 bx--col-xlg-6 trainerRow">
                            <div  className="bx--row">
                              <div className="bx--col-sm-1 bx--col-md-2 bx--col-lg-3 bx--col-xlg-3">
                                <p className="salon" data-type='title'>Salón</p>                            
                              </div>
                              <div className="bx--col-sm-2 bx--col-md-4 bx--col-lg-7 bx--col-xlg-7">
                                <p className="center-title"  data-type='title'>Horario</p>
                              </div>
                              <div className="bx--col-sm-1 bx--col-md-2 bx--col-lg-2 bx--col-xlg-2">
                                <p className=""  data-type='title'>Bicicleta</p>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </>
                    :
                    <>
                    <div className="bx--row">
                    <div className="bx--col-lg-4"></div>
                    <div className="bx--col-lg-4 centerTitle">
                        <h1>{this.state.name} no tiene reservaciones</h1>
                      </div>
                    <div className="bx--col-lg-4"></div>
                    </div>
                    </>
                  }

                  { this.state.data.map( (element, index) => {
                    return(

                      <div key={index} className="bx--grid">
                        <div className="bx--row">
                          <div className="bx--col-sm-2 bx--col-md-2 bx--col-lg-3 bx--col-xlg-3"></div>
                            <div className="bx--col-sm-4 bx--col-md-4 bx--col-lg-6 bx--col-xlg-6 row-padding">
                            <div className="bx--row">

                              <div className="bx--col-sm-1 bx--col-md-2 bx--col-lg-3 bx--col-xlg-3">
                                <p className="no-wrap">{(element.ClassRoomId) ? this.setClassRoomName(element.ClassRoomId) : null} </p>
                              </div>

                              <div className="bx--col-sm-2 bx--col-md-4 bx--col-lg-7 bx--col-xlg-7">
                                <p className="center-title">{element.ModifiedTime} - {element.Day} {element.Date} de {element.Month}</p>
                              </div>

                              <div className="bx--col-sm-1 bx--col-md-2 bx--col-lg-2 bx--col-xlg-2">
                                <b className="right">{element.Bike}</b>
                             
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      );
                    })
                  }
      </div>
        <br></br><br></br>
        <div className="bx--row">
        <div className="bx--col-lg-4"></div>
          <div className="bx--col-lg-4 centerTitle">
          
            <Link element={Link} to="/members">
              <Button size="small" kind="primary" renderIcon={Undo32}>Regresar</Button>
            </Link>
          </div>
        <div className="bx--col-lg-4"></div>
        </div>
      </div>
    );
  };
}


export default Reservations;
