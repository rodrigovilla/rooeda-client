import React from 'react';
import ls from 'local-storage'
import './PauseControl.scss';
import { Button } from 'carbon-components-react';
import { Modal } from 'react-responsive-modal';
import axios from 'axios';
import * as dateFns from "date-fns";
import { PauseFilled32, PlayFilledAlt32 } from '@carbon/icons-react';
import appSettings from '../../helpers/AppSettings';
import { toast } from 'react-toastify';

class PauseControl extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      data: [],
      statusEnabled: false,
      controlId: null,
      adminRole: ls.get('adminRole'),
      history: []
    };

    this.submit = this.submit.bind(this);
  }

  getInfo(){
    axios.all([
      axios.get(`${appSettings.SERVER_URL}/dashboard/pause-control/last`),
      axios.get(`${appSettings.SERVER_URL}/dashboard/pause-control/all-completed`)
    ]).then(axios.spread((control, history)  => {
      console.log(control);
        if(!control.data.control){
          this.setState({statusEnabled: false});
          return;
        }

        this.setState({data: control.data.control, statusEnabled: control.data.control.IsEnabled, history: history.data.control});
      })).catch(error => this.setState({ error, isLoading: false }));
  }

  componentDidMount() {
    this.getInfo();
  }

  onCloseModal = () => {
    this.setState({ open: false });
  };

  onOpenModal = () => {
    this.setState({ open: true });
  };

  submit(){
    var json = {
      Id: this.state.data._id,
      Status: this.state.statusEnabled,
      AdminRole: this.state.adminRole
    };

    axios.post(`${appSettings.SERVER_URL}/dashboard/pause-control`, json)
    .then(control => {
      if(control.data.control){
        toast(`Rooeda esta ${ (!this.state.statusEnabled) ? "Pausado" : "Activo" }`);
        this.setState({statusEnabled: !this.state.statusEnabled});
        this.getInfo();
      }else{
        toast(`Error`);
      }

      this.onCloseModal();
    }).catch(() => toast("Error"));
  }

  datediff(date1, date2) {
    var initial = new Date(date1);
    var finish = new Date(date2);
    var value = Math.round((finish-initial)/(1000*60*60*24));
    return (value > 0) ? value : 0;
  }

  render() {
    const { open } = this.state;
    return (
              <div>
              <div className="dashBoard">
                <div className="bx--grid">
                  <div className="bx--row">
                  <div className="bx--col-md-2"></div>
                  <div className="bx--col-md-4">
                      <div className="centerTitle">
                         <h3>Estado de Rooeda</h3>
                         {(this.state.statusEnabled) ? <h1 className="red-text">Pausado</h1> : <h1 className="green-text">Activo</h1> }
                      </div>
                      <div className="controlContainer">
                        <div className="bx--aspect-ratio bx--aspect-ratio--1x1 packBox" data-type='pause-control'>
                          <div className="bx--aspect-ratio--object">
                            <div className="buttonContent" onClick={this.onOpenModal}>
                            {(this.state.statusEnabled) ? <PlayFilledAlt32 /> : <PauseFilled32 />}
                            </div>
                          </div>
                        </div>
                        <br></br>
                        <div data-type='pause-control'>
                        <Button type="submit" value="Submit" className="buttonAccess" onClick={()=> window.location.href='/'}>
                        Regresar
                        </Button>
                        </div>
                      </div>
                  </div>
                  <div className="bx--col-md-2"></div>
                  </div>
                  
             {(this.state.history.length > 0)
                    ?
                    <>
                    <div className="bx--row">
                    <div className="bx--col-md-2"></div>
                    <div className="bx--col-md-4 centerTitle">
                      <br></br>
                      <br></br>
                      <h4>Historial</h4>
                      <br></br>
                      <br></br>
                    </div>
                    <div className="bx--col-md-2"></div>
                    </div>

                    <div className="bx--grid">
                        <div className="bx--row">
                          <div className="bx--col-sm-1 bx--col-md-2 bx--col-lg-3 bx--col-xlg-3"></div>
                            <div className="bx--col-sm-4 bx--col-md-4 bx--col-lg-6 bx--col-xlg-6 row-padding trainerRow">
                            <div  className="bx--row">

                              <div className="bx--col-sm-1 bx--col-md-2 bx--col-lg-3 bx--col-xlg-3">
                                <p data-type='title'>Dia Inicial</p>                            
                                 </div>

                              <div className="bx--col-sm-2 bx--col-md-4 bx--col-lg-7 bx--col-xlg-7">
                                <p className="center-title"  data-type='title'>Dia Final</p>
                              </div>

                              <div className="bx--col-sm-1 bx--col-md-2 bx--col-lg-2 bx--col-xlg-2">
                              <p className=""  data-type='title'>Dias Pausado</p>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </>
                    :
                    <>
                     <div className="bx--row">
                      <div className="bx--col-sm-4 bx--col-md-4 bx--col-lg-4 bx--col-xlg-4"></div>
                      <div className="bx--col-sm-4 bx--col-md-4 bx--col-lg-4 bx--col-xlg-4 centerTitle">
                        <br/>
                        <h4>No hay historial</h4>
                      </div>
                      <div className="bx--col-sm-4 bx--col-md-4 bx--col-lg-4 bx--col-xlg-4"></div>
                     </div>
                    </>
                  }

                  { this.state.history.map( (element, index) => {
                    return(

                      <div key={index} className="bx--grid">
                        <div className="bx--row">
                          <div className="bx--col-sm-2 bx--col-md-2 bx--col-lg-3 bx--col-xlg-3"></div>
                            <div className="bx--col-sm-4 bx--col-md-4 bx--col-lg-6 bx--col-xlg-6 row-padding">
                            <div className="bx--row">
                              <div className="bx--col-sm-1 bx--col-md-2 bx--col-lg-3 bx--col-xlg-3">
                                <p className="no-wrap">{dateFns.format(new Date(element.Created), 'yyyy-MM-dd') } </p>
                              </div>
                              <div className="bx--col-sm-2 bx--col-md-4 bx--col-lg-7 bx--col-xlg-7">
                                <b className="center pt">{ (element.Updated) ? dateFns.format(new Date(element.Updated), 'yyyy-MM-dd') : '' }</b>
                              </div>
                              <div className="bx--col-sm-1 bx--col-md-2 bx--col-lg-2 bx--col-xlg-2">
                                <b className="right">{ (element.Updated) ?  this.datediff(element.Created, element.Updated): ''}</b>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      );
                    })
                  }
                </div>
              </div>
              <Modal 
              open={open} 
              onClose={this.onCloseModal} 
              center
              animationDuration={80}
              >

             <p>¿Realmente deseas {(!this.state.statusEnabled) ? "pausar" : "activar" } Rooeda?</p>
             <br></br>
             <div className="padding-center">
             <Button 
                type="submit" 
                value="Submit" 
                size='small'
                kind='ghost' 
                onClick={this.submit}
              >
                Confirmar
              </Button>
             </div>
            </Modal>
              </div>
            );
};
}

export default PauseControl;
