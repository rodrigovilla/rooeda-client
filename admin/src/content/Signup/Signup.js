import React from 'react';
import '../../components/LoginControl/LoginControl.scss?v=2.0.0';
import { Form, TextInput, Button, Toggle, Checkbox } from 'carbon-components-react';
import ls from 'local-storage'
import axios from 'axios';
import appSettings from '../../helpers/AppSettings';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const NameInputProps = {
  className: 'name',
  id: 'name',
  labelText: 'Nombre',
  placeholder: ''
};
const EmailInputProps = {
  className: 'age',
  id: 'email',
  labelText: 'Correo electrónico',
  placeholder: ''
};

const PasswordInputProps = {
  className: 'password',
  id: 'pass',
  labelText: 'Contraseña',
  placeholder: 'Mínimo 6 caracteres + 1 dígito'
};


class SignupForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {name: '', email: '', password: '', isAdmin: true, showPass: false};

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    let nam = event.target.name;
    let val = event.target.value;
    this.setState({[nam]: val});
  }

  handleSubmit(event) {
    event.preventDefault();
    let currentState = this;
    let values =  { Name: this.state.name, Email: this.state.email, Password: this.state.password, IsAdmin: this.state.isAdmin , AdminRole: ls.get('adminRole')};
 
    axios.post(`${appSettings.SERVER_URL}/admin/validate-mail`, { Email:this.state.email})
    .then( existEmail => {
        if(!existEmail.data.Available){
          //--- Toast Failed. ya existe correo electronico
          toast("El email ya existe");
          return;
        }else{
          axios.post(`${appSettings.SERVER_URL}/admin/add`, values)
          .then(function (response) {
              //--- Toast succes. Usuario Añadido correctamente
              currentState.setState({name: '', email: '', password: '', isAdmin: true});
              toast("Cuenta creada correctamente");
              setTimeout(() => {
              window.location.href = '/';
              }, 4000);
          }).catch(error => {
            //--- Toast fail. Inicia session
            toast("Error al crear cuenta");
          });
        }
    }).catch(error => {
       //--- Toast fail. Email error
       toast("Email incorrecto");
    });


  }

  render() {
    return (
      <div className="dashBoard">
      <div className="formStyle">
      <div className="bx--grid">
      <div className="bx--row">
        <div className="bx--col-lg-4 bx--col-md-2 bx--col-sm-1"></div>
        <div className="bx--col-lg-4 bx--col-md-4 bx--col-sm-4">
          <Form onSubmit={this.handleSubmit}>

              <h3 className="formTitles">
                Crear cuenta
              </h3>

              <TextInput
              name='name'
              required
              type="text" 
              value={this.state.name} 
              onChange={this.handleChange} 
              {...NameInputProps}  
              />


              <TextInput
              name='email'
              type="email" 
              required
              value={this.state.email} 
              onChange={this.handleChange} 
              pattern="^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+"
              
              {...EmailInputProps}  
              />

              <TextInput
              name='password'
              type={ !this.state.showPass ? 'password' : 'text'}
              required
              value={this.state.password} 
              onChange={this.handleChange} 
              pattern="(?=.*\d)(?=.*[a-z]).{7,}"

              {...PasswordInputProps}  
              />
              <Checkbox labelText="Mostrar contraseña." id="checked" onChange={() => this.setState({showPass: !this.state.showPass})} />
              <br></br>

              <Toggle
                name='isAdmin'
                aria-label="toggle button"
                defaultToggled
                id="toggle-1"
                labelText="Cuenta de administrador"
                labelA= "Inactivo"
                labelB= "Activo"
                value={this.state.isAdmin} 
                onChange={() => this.setState({isAdmin: !this.state.isAdmin})} 
              />
              <br></br>
              <Button type="submit" value="Submit" className="buttonAccess">Registrarse</Button>
          </Form>


        </div>  
        <div className="bx--col-lg-4 bx--col-md-2 bx--col-sm-1"></div>
      </div>
      </div>
      </div>
      </div>
    );
  }
}

export default SignupForm;