import React from 'react';
import './ClassRoomContent.scss?v=2.0.0';
import { Link } from 'react-router-dom';
import { Button } from 'carbon-components-react';
import { Undo32 } from '@carbon/icons-react';
import axios from 'axios';
import 'react-responsive-modal/styles.css';
import appSettings from '../../helpers/AppSettings';
import ls from 'local-storage'
import 'react-toastify/dist/ReactToastify.css';

class ClassRoomContent extends React.Component {
  constructor(props) {
    super(props);
    var classRoomContent =  ls.get('classRoomContent');
    this.state = {
      data: [],
      time: classRoomContent.time,
      id: classRoomContent.id,
      date: classRoomContent.date,
      day: classRoomContent.day,
      number: classRoomContent.number,
      modifiedTime: classRoomContent.modifiedTime
    };
  }

  componentDidMount() {
    axios.post(`${appSettings.SERVER_URL}/admin-schedule`, this.state)
      .then(response => this.setState({ data: response.data }))
      .catch();
  }

  render() {
    return (
    <>

    <div className="dashBoard">
    <div className="formStyle">
            
      <div className="bx--grid">
        <div className="bx--row">
          <div className="bx--col-lg-4"></div>
          <div className="bx--col-lg-4 centerTitle">
            <h1>{this.state.day} {this.state.number}</h1>
            <h3>{this.state.modifiedTime}</h3>
          </div>
          <div className="bx--col-lg-4"></div>
        </div>

        <div className="bx--row">
                    <div className="bx--col-md-2"></div>
                    <div className="bx--col-md-4 trainerRow">
                      <p data-type='title'>
                          Nombre
                      </p>
                      <p className="bikeNumber"  data-type='title'>
                          Bicicleta
                      </p>
                    </div>
                    <div className="bx--col-md-2"></div>
        </div>
          

        { this.state.data.map( (element, index) => {
          return(
            
            <div key={index} className="bx--row">
           
                <div className="bx--col-md-2"></div>
                <div className="bx--col-md-4 trainerRow">
                  <p>{element.Name}</p>
                  <p className="overflowMenu">{element.BikeId}</p>
                </div>
                <div className="bx--col-md-2"></div>
            </div>
            );
          })
        }

        <div className="bx--row paddingButton">
          <div className="bx--col-lg-4"></div>
          <div className="bx--col-lg-4 centerTitle">
          <Link element={Link} to="/calendar-class">
          <Button 
            size="small"
            kind="secondary"
            renderIcon={Undo32}
          >Regresar
          </Button>
          </Link>
          </div>
          <div className="bx--col-lg-4"></div>
        </div>
      </div>
    </div>
    </div>
    </>
    );
  }
};
export default ClassRoomContent;
