import React from 'react';
import './Settings.scss';
import { Link } from 'react-router-dom';
import { Button, Toggle } from 'carbon-components-react';
import { Checkmark32, MisuseOutline32, TrashCan32 } from '@carbon/icons-react';
import axios from 'axios';
import 'react-responsive-modal/styles.css';
import { Modal } from 'react-responsive-modal';
import appSettings from '../../helpers/AppSettings';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

class Instructors extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      open: false,
      selected: ''
    };
  }


  onOpenModal = () => {
    this.setState({ open: true });
  };
 
  onCloseModal = () => {
    this.setState({ open: false });
  };

  componentDidMount() {
    axios.get(`${appSettings.SERVER_URL}/instructor/all`)
      .then(response => this.setState({ data: response.data }))
      .catch();
  }

  delete(id){
    this.setState({open: false});
    axios.delete(`${appSettings.SERVER_URL}/instructor/${id}`)
      .then(response => {
        if(response.status === 200){
          axios.get(`${appSettings.SERVER_URL}/instructor/all`)
          .then(response => this.setState({ data: response.data }))
          .catch();
        }else{
          //---- Error
          toast("No se pudo borrar el instructor");
        }
      }).catch();
  }

  edit(id){
    window.location.href=`/addinstructor/${id}`;
  }

  setInstructor(id){
    this.onOpenModal();
    this.setState({selected: id});
  }


  render() {
    const { open } = this.state;
    return (
    <>
     <Modal 
              open={open} 
              onClose={this.onCloseModal} 
              center
              animationDuration={80}
              >

             <p>¿Realmente deseas de eliminar este instructor?</p>
             <br></br>
             <Button 
                renderIcon={MisuseOutline32 }  
                type="submit" 
                value="Submit" 
                size='small'
                kind='ghost' 
                onClick={this.onCloseModal}
              >
                Cancelar
              </Button>
              <Button 
                renderIcon={TrashCan32}  
                type="submit" 
                value="Submit" 
                size='small'
                kind='ghost' 
                onClick={() => this.delete(this.state.selected)}
              >
                Eliminar
              </Button>

            </Modal>
    <div className="dashBoard">
    <div className="formStyle">
      <div className="bx--grid">
        <div className="bx--row">
          <div className="bx--col-lg-4"></div>
          <div className="bx--col-lg-4 centerTitle">
            <h1>Configuración</h1>
          </div>
          <div className="bx--col-lg-4"></div>
        </div>

        <div className="bx--row">
                    <div className="bx--col-md-2"></div>
                    <div className="bx--col-md-4 trainerRow">
                      <p data-type='title'>
                          Sistema
                      </p>
                      <p className="bikeNumber"  data-type='title'>
                      </p>
                    </div>
                    <div className="bx--col-md-2"></div>
        </div>
          

        { this.state.data.map( (element, index) => {
          return(
            
            <div key={index} className="bx--row">
           
                <div className="bx--col-md-2"></div>
                <div className="bx--col-md-4 trainerRow">

                  <Toggle
                      name='isAdmin'
                      aria-label="toggle button"
                      defaultToggled
                      id="toggle-1"
                      labelText="Estado actual del sistema"
                      labelA= "Inactivo"
                      labelB= "Activo"
                      value={this.state.isAdmin} 
                      onChange={() => this.setState({isAdmin: !this.state.isAdmin})} 
                  />
                </div>
                <div className="bx--col-md-2"></div>
            </div>
            );
          })
        }

        <div className="bx--row">
          <div className="bx--col-md-2"></div>
          <div className="bx--col-md-4 trainerRow">
            <p data-type='title'>
                Historial
            </p>
            <p className="bikeNumber"  data-type='title'>
              l
            </p>
          </div>
          <div className="bx--col-md-2"></div>
        </div>

        <div className="bx--row">
          <div className="bx--col-md-2"></div>
          <div className="bx--col-md-4">
              <div className="settings">
                  <p>Día inicial: date</p>
              </div>
          </div>
          <div className="bx--col-md-2"></div>
        </div>


        <div className="bx--row">
          <div className="bx--col-md-2"></div>
          <div className="bx--col-md-4">
              <div className="settings">
                  <p>Día final: date</p>
              </div>
          </div>
          <div className="bx--col-md-2"></div>
        </div>

        <div className="bx--row">
          <div className="bx--col-md-2"></div>
          <div className="bx--col-md-4">
              <div className="settings">
                  <p>Días pausado: date</p>
              </div>
          </div>
          <div className="bx--col-md-2"></div>
        </div>

        <div className="bx--row paddingButton">
          <div className="bx--col-lg-4"></div>
          <div className="bx--col-lg-4 centerTitle">
          <Link element={Link} to="/addinstructor">
          <Button 
            size="small"
            kind="secondary"
            renderIcon={Checkmark32}
          > Guardar
          </Button>
          </Link>
          </div>
          <div className="bx--col-lg-4"></div>
        </div>
      </div>
    </div>
    </div>
    </>
    );
  }
};
export default Instructors;
