import React from 'react';
import './Packs.scss';
import { ChevronLeft32  } from '@carbon/icons-react';
import axios from 'axios';
import appSettings from '../../helpers/AppSettings';
import { Button , Search } from 'carbon-components-react';
import ls from 'local-storage';

class Members extends React.Component {
  constructor(props) {
    super(props);
    this.state = { data: [], auxList: [], text: '' };
  }

  componentDidMount() {
      axios.get(`${appSettings.SERVER_URL}/users/all`)
      .then(users =>  this.setState({ data: users.data, auxList: users.data }))
      .catch(error => this.setState({ error, isLoading: false }));
  }

  datediff(ex) {
    var now = new Date();
    var today = Date.UTC(now.getFullYear(), now.getUTCMonth(), now.getDate())
    var expire = new Date(ex);
    return Math.round((expire-today)/(1000*60*60*24));
  }

  filter(text){
    if(text.target.value){
      var result = this.state.data.filter(x => {
        return ((x.Name) ? x.Name.toLowerCase().includes(text.target.value.toLowerCase()) : false)
        || ((x.Email)? x.Email.toLowerCase().includes(text.target.value.toLowerCase()): false)
      });

      this.setState({auxList: result})
      return;
    }

    this.setState({ auxList: this.state.data })
  }

  render(){
    return (
      <div className="dashBoard">
      <div className="formStyle">
        <div className="bx--grid">
          <div className="bx--row">
            <div className="bx--col-lg-3"></div>
            <div className="bx--col-lg-6 bx--no-gutter">
            <Search
                  id="search-1"
                  name='search'
                  placeHolderText="Buscar"
                  light
                  onChange={text => this.filter(text)}
                />
                <br></br>
            </div>
            <div className="bx--col-lg-3"></div>
          </div>
          <div class="bx--row">
                    <div class="bx--col-md-2"></div>
                    <div class="bx--col-md-4 trainerRow">
                      <p data-type='title'>
                          Nombre
                      </p>
                      <p className="bikeNumber"  data-type='title'>
                          Correo
                      </p>
                    </div>
                    <div class="bx--col-md-2"></div>
          </div>
          
          { this.state.auxList.map( (element, index) => {
                return(
                  <div key={index} className="bx--row">
                  <div className="bx--col-md-2"></div>
                  <div className="bx--col-md-2 trainerRow" >
                    <p
                      className="instructorsLink"
                      onClick={ () => {
                        ls.set("customerId", element.Id);
                        ls.set("customerName", element.Name);
                        ls.set("customerEmail", element.Email ? element.Email : '');
                        window.location.href = "/select-pack";
                      }}
                    >{element.Name}</p>
                  </div>

                  <div className="bx--col-md-2 trainerRow" >
                  <div>
                  <p
                      className="instructorsLink right"
                      onClick={ () => {
                        ls.set("customerId", element.Id);
                        ls.set("customerName", element.Name);
                        ls.set("customerEmail", element.Email);
                        window.location.href = "/select-pack";
                      }}
                    >{element.Email}</p>
                    </div>
                    </div>
                  <div className="bx--col-md-2"></div>
                  </div>
                );
              })
          }
        </div>

        <div className="bx--row paddingButton">
          <div className="bx--col-lg-4"></div>
          <div className="bx--col-lg-4 centerTitle">

          <Button 
            size="small"
            kind="secondary"
            renderIcon={ChevronLeft32}
            onClick={() => window.location.href = "/"}
          >
            Regresar
          </Button>

          </div>
          <div className="bx--col-lg-4"></div>
        </div>

        </div>
      </div>
    );
  };
}

export default Members;
