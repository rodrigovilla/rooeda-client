import React from 'react';
import './Packs.scss';
import ls from 'local-storage'
import axios from 'axios';
import appSettings from '../../helpers/AppSettings';
import { toast } from 'react-toastify';
import { Modal } from 'react-responsive-modal';
import { Button } from 'carbon-components-react';
import { Checkmark32 } from '@carbon/icons-react';
import packList from '../../helpers/pack-type';

class Packs extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      butonsDisabled: false,
      modalOpen: false,
      customerName:  ls.get("customerName"),
      customerEmail: ls.get("customerEmail"),
      customerId: ls.get("customerId"),
      totalClases: 0,
      pack: 0
    };

  }

  handleSubmit(e){
    this.setState({butonsDisabled: true});
    const customerId = ls.get('customerId');

    if(!customerId){
      window.location.href='/add-pack'
      return;
    }

    if (e !== 0){
      this.addPack(customerId, e)
      return;
    }

    axios.get(`${appSettings.SERVER_URL}/user-pack/pack-zero/${customerId}`)
      .then( packs  => {
        if(packs.data.Found){
          toast('Se termino el limite de clases de prueba');
          this.setState({butonsDisabled: false});
        }else{
          this.addPack(customerId, e)
        }
      }).catch(error => this.setState({ error }));
  }



  addPack(){
    this.setState({modalOpen:false});
    let json = {
      UserId: this.state.customerId,
      ClassId: this.state.pack
    };

    axios.post(`${appSettings.SERVER_URL}/user-pack`, json)
    .then(function (response) {
        //--- Toast notification
        toast("Pack agregado");
        ls.remove('customerId');
        ls.remove('customerName');
        ls.remove('customerEmail');
        setTimeout(() => {
          window.location.href='/'
          }, 4000);
    })
    .catch(function (error) {
        //--- Toast de error
        toast("Error!");
         this.setState({butonsDisabled: false});
    });
  }

  onOpenModal (packType) {
    if(packType !== 0){
      this.setState({ modalOpen: true, butonsDisabled: true, totalClases: packList[packType].Clases, pack : packType });
      return;
    }

    axios.get(`${appSettings.SERVER_URL}/user-pack/pack-zero/${this.state.customerId}`)
      .then( packs  => {
        if(packs.data.Found){
          toast('Se termino el limite de clases de prueba para: ' + this.state.customerEmail );
          this.setState({butonsDisabled: false});
        }else{
          this.setState({ modalOpen: true, butonsDisabled: true, totalClases: packList[packType].Clases, pack : packType });
        }
      }).catch(error => this.setState({ error }));

  };

  onCloseModal = () => {
    this.setState({ modalOpen: false, butonsDisabled: false });
  };

  render() {
    return (
      <>
      <Modal 
            open={this.state.modalOpen} 
            onClose={this.onCloseModal} 
            center
            animationDuration={80}
            modalId={3} 
            >
               <span className="bx--label padding-center">Añadir {this.state.totalClases} { (this.state.totalClases > 1) ? "clases": "clase" } a {this.state.customerName} ({this.state.customerEmail})</span>

         <div className="padding-center">
              <Button 
                renderIcon={Checkmark32}  
                type="submit" 
                value="Submit" 
                size='small'
                kind='ghost' 
                onClick= {() => this.addPack()}
              >
                Confirmar
              </Button>
            </div>
        </Modal>

      <div className="bx--grid bx--grid--full-width landing-page dashBoard pickupPackContainer">
      <div className="bx--row">
        <div class="bx--col">
          <div className="packTitle">
            <h1>Seleccionar Pack</h1>
          </div>
        </div>
      </div>
      
      { (!this.state.butonsDisabled) ?
       <div className="pickupPack">
       {/* Row starts */}
       <div className="bx--row">
           <div className="bx--col-md-4 bx--col-lg-2"></div>
           <div className="bx--col-md-4 bx--col-lg-2"></div>
           <div className="bx--col-md-4 bx--col-lg-2">
               <div className="bx--aspect-ratio bx--aspect-ratio--1x1 packBox" >
               <div className="bx--aspect-ratio--object" onClick={() => this.onOpenModal(0)}>
                 <div className="packContent" >
                   <h2>1er</h2>
                   <h4>Clase</h4>
                 <div className="packContentBottom">
                   <p>$90</p>
                   <span>Expira en 1 día</span>
                 </div>
                 </div>
               </div>  
               </div> 
           </div>
           <div className="bx--col-md-4 bx--col-lg-2">
               <div className="bx--aspect-ratio bx--aspect-ratio--1x1 packBox" >
               <div className="bx--aspect-ratio--object">
                 <div className="packContent" onClick={() => this.onOpenModal(1)}>
                   <h2>1</h2>
                   <h4>Clase</h4>
                 <div className="packContentBottom">
                   <p>$120</p>
                   <span>Expira en 5 días</span>
                 </div>
                 </div>
               </div>  
               </div> 
           </div>
           <div className="bx--col-md-4 bx--col-lg-2"></div>
           <div className="bx--col-md-4 bx--col-lg-2"></div>
       </div>
 
       {/* Row starts */}
       <div className="bx--row">
           <div className="bx--col-md-4 bx--col-lg-2"></div>
           <div className="bx--col-md-4 bx--col-lg-2"></div>
           <div className="bx--col-md-4 bx--col-lg-2">
               <div className="bx--aspect-ratio bx--aspect-ratio--1x1 packBox" >
               <div className="bx--aspect-ratio--object">
                 <div className="packContent" onClick={() => this.onOpenModal(2)}>
                   <h2>5</h2>
                   <h4>Clases</h4>
                 <div className="packContentBottom">
                   <p>$550</p>
                   <span>Expira en 15 días</span>
                 </div>
                 </div>
               </div>  
               </div> 
           </div>
           <div className="bx--col-md-4 bx--col-lg-2">
               <div className="bx--aspect-ratio bx--aspect-ratio--1x1 packBox" >
               <div className="bx--aspect-ratio--object">
                 <div className="packContent" onClick={() => this.onOpenModal(3)}>
                   <h2>10</h2>
                   <h4>Clases</h4>
                 <div className="packContentBottom">
                   <p>$1,000</p>
                   <span>Expira en 30 días</span>
                 </div>
                 </div>
               </div>  
               </div> 
           </div>
           <div className="bx--col-md-4 bx--col-lg-2"></div>
           <div className="bx--col-md-4 bx--col-lg-2"></div>
       </div>
 
       {/* Row starts */}
       <div className="bx--row">
           <div className="bx--col-md-4 bx--col-lg-2"></div>
           <div className="bx--col-md-4 bx--col-lg-2"></div>
           <div className="bx--col-md-4 bx--col-lg-2">
               <div className="bx--aspect-ratio bx--aspect-ratio--1x1 packBox" >
               <div className="bx--aspect-ratio--object">
                 <div className="packContent" onClick={() => this.onOpenModal(4)}>
                   <h2>15</h2>
                   <h4>Clases</h4>
                 <div className="packContentBottom">
                   <p>$1,350</p>
                   <span>Expira en 40 días</span>
                 </div>
                 </div>
               </div>  
               </div> 
           </div>
           <div className="bx--col-md-4 bx--col-lg-2">
               <div className="bx--aspect-ratio bx--aspect-ratio--1x1 packBox" >
               <div className="bx--aspect-ratio--object">
                 <div className="packContent" onClick={() => this.onOpenModal(5)}>
                   <h2>25</h2>
                   <h4>Clases</h4>
                 <div className="packContentBottom">
                   <p>$2,050</p>
                   <span>Expira en 60 días</span>
                 </div>
                 </div>
               </div>  
               </div> 
           </div>
           <div className="bx--col-md-4 bx--col-lg-2"></div>
           <div className="bx--col-md-4 bx--col-lg-2"></div>
       </div>
 
       {/* Row starts */}
       <div className="bx--row">
           <div className="bx--col-md-4 bx--col-lg-2"></div>
           <div className="bx--col-md-4 bx--col-lg-2"></div>
           <div className="bx--col-md-4 bx--col-lg-2">
               <div className="bx--aspect-ratio bx--aspect-ratio--1x1 packBox" >
               <div className="bx--aspect-ratio--object">
                 <div className="packContent" onClick={() => this.onOpenModal(6)}>
                   <h2>50</h2>
                   <h4>Clases</h4>
                 <div className="packContentBottom">
                   <p>$3,750</p>
                   <span>Expira en 105 días</span>
                 </div>
                 </div>
               </div>  
               </div> 
           </div>
           <div className="bx--col-md-4 bx--col-lg-2">
               <div className="bx--aspect-ratio bx--aspect-ratio--1x1">
               <div className="bx--aspect-ratio--object">
               </div>  
               </div> 
           </div>
           <div className="bx--col-md-4 bx--col-lg-2"></div>
           <div className="bx--col-md-4 bx--col-lg-2"></div>
       </div>
       </div>
      :
      <div className="">
      {/* Row starts */}
      <div className="bx--row">
          <div className="bx--col-md-4 bx--col-lg-2"></div>
          <div className="bx--col-md-4 bx--col-lg-2"></div>
          <div className="bx--col-md-4 bx--col-lg-2">
              <div className="bx--aspect-ratio bx--aspect-ratio--1x1 packBox" >
              <div className="bx--aspect-ratio--object opacity">
                <div className="packContent" >
                  <h2>1er</h2>
                  <h4>Clase</h4>
                <div className="packContentBottom">
                  <p>$90</p>
                  <span>Expira en 1 día</span>
                </div>
                </div>
              </div>  
              </div> 
          </div>
          <div className="bx--col-md-4 bx--col-lg-2">
              <div className="bx--aspect-ratio bx--aspect-ratio--1x1 packBox" >
              <div className="bx--aspect-ratio--object opacity">
                <div className="packContent">
                  <h2>1</h2>
                  <h4>Clase</h4>
                <div className="packContentBottom">
                  <p>$120</p>
                  <span>Expira en 5 días</span>
                </div>
                </div>
              </div>  
              </div> 
          </div>
          <div className="bx--col-md-4 bx--col-lg-2"></div>
          <div className="bx--col-md-4 bx--col-lg-2"></div>
      </div>

      {/* Row starts */}
      <div className="bx--row">
          <div className="bx--col-md-4 bx--col-lg-2"></div>
          <div className="bx--col-md-4 bx--col-lg-2"></div>
          <div className="bx--col-md-4 bx--col-lg-2">
              <div className="bx--aspect-ratio bx--aspect-ratio--1x1 packBox" >
              <div className="bx--aspect-ratio--object opacity">
                <div className="packContent">
                  <h2>5</h2>
                  <h4>Clases</h4>
                <div className="packContentBottom">
                  <p>$550</p>
                  <span>Expira en 15 días</span>
                </div>
                </div>
              </div>  
              </div> 
          </div>
          <div className="bx--col-md-4 bx--col-lg-2">
              <div className="bx--aspect-ratio bx--aspect-ratio--1x1 packBox" >
              <div className="bx--aspect-ratio--object opacity">
                <div className="packContent">
                  <h2>10</h2>
                  <h4>Clases</h4>
                <div className="packContentBottom">
                  <p>$1,000</p>
                  <span>Expira en 30 días</span>
                </div>
                </div>
              </div>  
              </div> 
          </div>
          <div className="bx--col-md-4 bx--col-lg-2"></div>
          <div className="bx--col-md-4 bx--col-lg-2"></div>
      </div>

      {/* Row starts */}
      <div className="bx--row">
          <div className="bx--col-md-4 bx--col-lg-2"></div>
          <div className="bx--col-md-4 bx--col-lg-2"></div>
          <div className="bx--col-md-4 bx--col-lg-2">
              <div className="bx--aspect-ratio bx--aspect-ratio--1x1 packBox" >
              <div className="bx--aspect-ratio--object opacity">
                <div className="packContent">
                  <h2>15</h2>
                  <h4>Clases</h4>
                <div className="packContentBottom">
                  <p>$1,350</p>
                  <span>Expira en 40 días</span>
                </div>
                </div>
              </div>  
              </div> 
          </div>
          <div className="bx--col-md-4 bx--col-lg-2">
              <div className="bx--aspect-ratio bx--aspect-ratio--1x1 packBox" >
              <div className="bx--aspect-ratio--object opacity">
                <div className="packContent">
                  <h2>25</h2>
                  <h4>Clases</h4>
                <div className="packContentBottom">
                  <p>$2,050</p>
                  <span>Expira en 60 días</span>
                </div>
                </div>
              </div>  
              </div> 
          </div>
          <div className="bx--col-md-4 bx--col-lg-2"></div>
          <div className="bx--col-md-4 bx--col-lg-2"></div>
      </div>

      {/* Row starts */}
      <div className="bx--row">
          <div className="bx--col-md-4 bx--col-lg-2"></div>
          <div className="bx--col-md-4 bx--col-lg-2"></div>
          <div className="bx--col-md-4 bx--col-lg-2">
              <div className="bx--aspect-ratio bx--aspect-ratio--1x1 packBox" >
              <div className="bx--aspect-ratio--object opacity">
                <div className="packContent">
                  <h2>50</h2>
                  <h4>Clases</h4>
                <div className="packContentBottom">
                  <p>$3,750</p>
                  <span>Expira en 105 días</span>
                </div>
                </div>
              </div>  
              </div> 
          </div>
          <div className="bx--col-md-4 bx--col-lg-2">
              <div className="bx--aspect-ratio bx--aspect-ratio--1x1">
              <div className="bx--aspect-ratio--object">
              </div>  
              </div> 
          </div>
          <div className="bx--col-md-4 bx--col-lg-2"></div>
          <div className="bx--col-md-4 bx--col-lg-2"></div>
      </div>
      </div>
      }
     <div className="bx--row">
     <div className="bx--col-md-2 bx--col-lg-4"></div>
     <div className="bx--col-md-4 bx--col-lg-4">
     <Button 
              type="submit" 
              value="Submit" 
              className="buttonAccess"
              size="small"
              kind="secondary"
              onClick={() => window.location.href = "/add-pack"}
          >
              Regresar
          </Button>

     </div>
     <div className="bx--col-md-2 bx--col-lg-4">
     </div>
     </div>
     
      {/* Grid ends */}
      </div>
      </>
    );
  } 
}
export default Packs;
