import React from 'react';
import './ClassRoom.scss?v=2.0.0';
import { Link } from 'react-router-dom';
import { OverflowMenu, OverflowMenuItem } from 'carbon-components-react';
import { Button } from 'carbon-components-react';
import { MisuseOutline32  , TrashCan32, Add32} from '@carbon/icons-react';
import axios from 'axios';
import 'react-responsive-modal/styles.css';
import { Modal } from 'react-responsive-modal';
import appSettings from '../../helpers/AppSettings';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

class ClassRooms extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      open: false,
      selected: ''
    };
  }


  onOpenModal = () => {
    this.setState({ open: true });
  };
 
  onCloseModal = () => {
    this.setState({ open: false });
  };

  componentDidMount() {
    axios.get(`${appSettings.SERVER_URL}/class-room/all`)
      .then(response => this.setState({ data: response.data }))
      .catch();
  }

  delete(id){
    this.setState({open: false});
    axios.delete(`${appSettings.SERVER_URL}/class-room/${id}`)
      .then(response => {
        if(response.status === 200){
          axios.get(`${appSettings.SERVER_URL}/class-room/all`)
          .then(response => this.setState({ data: response.data }))
          .catch();
        }else{
          //---- Error
          toast("No se pudo borrar el salón");
        }
      }).catch();
  }

  edit(id){
    window.location.href=`/add-class-room/${id}`;
  }

  setInstructor(id){
    this.onOpenModal();
    this.setState({selected: id});
  }


  render() {
    const { open } = this.state;
    return (
    <>
     <Modal 
              open={open} 
              onClose={this.onCloseModal} 
              center
              animationDuration={80}
              >
             <h4>¿Realmente deseas de eliminar este salón?</h4> 
             <br></br>
             <p>Esto puede causar conflictos y perdidas de información en la aplicación</p>
             <br></br>

             <Button 
                renderIcon={MisuseOutline32 }  
                type="submit" 
                value="Submit" 
                kind='ghost' 
                size='small'
                onClick={this.onCloseModal}
              >
                Cancelar
              </Button>
              &nbsp;&nbsp;&nbsp;
                <Button
                className="button-delete" 
                renderIcon={TrashCan32}  
                type="submit" 
                value="Submit" 
                kind='ghost'
                size='small'
                onClick={() => this.delete(this.state.selected)}
              >
                Eliminar
              </Button>
             
              

            </Modal>
    <div className="dashBoard">
    <div className="formStyle">
            
      <div className="bx--grid">
        <div className="bx--row">
          <div className="bx--col-lg-4"></div>
          <div className="bx--col-lg-4 centerTitle">
            <h1>Salónes</h1>
          </div>
          <div className="bx--col-lg-4"></div>
        </div>

        <div className="bx--row">
                    <div className="bx--col-md-2"></div>
                    <div className="bx--col-md-4 trainerRow">
                      <p data-type='title'>
                          Nombre
                      </p>
                      <p className="bikeNumber"  data-type='title'>
                          Modificar
                      </p>
                    </div>
                    <div className="bx--col-md-2"></div>
        </div>
          

        { this.state.data.map( (element, index) => {
          return(
            
            <div key={index} className="bx--row">
           
                <div className="bx--col-md-2"></div>
                <div className="bx--col-md-4 trainerRow">
                  <p 
                    className="instructorsLink"
                    onClick={() => this.edit(element._id)}>
                      {element.Name}
                  </p>
                  <OverflowMenu className="overflowMenu" flipped>
                  <OverflowMenuItem 
                      itemText="Editar" 
                      onClick={() => this.edit(element._id)}
                  />
                  <OverflowMenuItem 
                      itemText="Eliminar" 
                      hasDivider 
                      isDelete 
                      onClick={() => this.setInstructor(element._id)}
                  />
                  </OverflowMenu>
                </div>
                <div className="bx--col-md-2"></div>
            </div>
            );
          })
        }

        <div className="bx--row paddingButton">
          <div className="bx--col-lg-4"></div>
          <div className="bx--col-lg-4 centerTitle">
          <Link element={Link} to="/add-class-room">
              <Button 
                size="small"
                kind="secondary"
                renderIcon={Add32}
              >Agregar salón
              </Button>
          </Link>
          </div>
          <div className="bx--col-lg-4"></div>
        </div>
      </div>
    </div>
    </div>
    </>
    );
  }
};
export default ClassRooms;
