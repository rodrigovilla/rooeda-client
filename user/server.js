//server.js
const express = require('express');
const favicon = require('express-favicon');
const path = require('path');
const port = process.env.PORT || 8000;
const app = express();
app.use(favicon(__dirname + '/build/favicon.ico'));
var proxy = require('http-proxy-middleware');
require('dotenv').config();
let SERVER_URL = process.env.SERVER_URL;

// the __dirname is the current directory from where the script is running
app.use(express.static(__dirname));
app.use(express.static(path.join(__dirname, 'build')));

app.use(
  proxy('/api', { 
    target: SERVER_URL, 
    changeOrigin: true ,
    secure: true
  })
);

app.get('/*', function (req, res) {
  res.sendFile(path.join(__dirname, 'build', 'index.html'));
});

app.listen(port);