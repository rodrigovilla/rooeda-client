export default [
  {
      Id: 0,
      Price: 90,
      Clases: 1,
      Expires: 1
  },
  {
      Id: 1,
      Price: 120,
      Clases: 1,
      Expires: 5
  },
  {
      Id: 2,
      Price: 550,
      Clases: 5,
      Expires: 15
  },
  {
      Id: 3,
      Price: 1000,
      Clases: 10,
      Expires: 30
  },
  {
      Id: 4,
      Price: 1350,
      Clases: 15,
      Expires: 50
  },
  {
      Id: 5,
      Price: 2050,
      Clases: 25,
      Expires: 90
  },
  {
      Id: 6,
      Price: 3750,
      Clases: 50,
      Expires: 180
  }
]