export default {
  SERVER_URL: '/api',
  STRIPE_SCRIPT_URL: 'https://js.stripe.com/v3/',
  MERCADO_PAGO_SCRIPT_URL: 'https://sdk.mercadopago.com/js/v2'
};
