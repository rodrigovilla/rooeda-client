import React from 'react';
import axios from 'axios';
import ls from 'local-storage'
import appSettings from '../../helpers/AppSettings';

class NameForm extends React.Component {
  constructor(props) {
    super(props);
    
    let id = props.location.pathname.split('/').pop();

    axios.get(`${appSettings.SERVER_URL}/users/id/${id}`)
      .then(res => {
            if(res.status !== 404){
                ls.set('session', res.data.Id);
                ls.set('name', res.data.Name);
                window.location.href='/profile';
            }else{
              window.location.href='/';
            }   
      }).catch(function (error) { window.location.href='/'; });
  }
  
  render() {
      return (<p>Redirecting...</p>)
  }
}

export default NameForm;