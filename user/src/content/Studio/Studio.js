import React from 'react';
import './Studio.scss';
import IconFacebook from './icon-facebook.svg';
import IconInstagram from './icon-instagram.svg';
import { Link } from 'react-router-dom';
import { Button } from 'carbon-components-react';
import { Cyclist32, EventSchedule32 } from '@carbon/icons-react';
import Footer from '../../components/Footer';
import ls from 'local-storage'

const renderAuthButton = ()=>{
  let session = ls.get('session');
  if(session == null){
    return <Link element={Link} to="/signup"><Button renderIcon={Cyclist32}>Regístrate ahora</Button></Link>
  } else{
    return <Link element={Link} to="/calendar"><Button renderIcon={EventSchedule32}>Calendario</Button></Link>
  }
}


class Studio extends React.Component  {
  render() {
  return (
    <>
    
      <div className="studioBox">
      <div className="bx--grid">
        <div className="bx--row">

          <div className="bx--col-sm-8 bx--col-lg-6">
            <h2 className="studioCopy">
            El studio es una experiencia, buscamos inspirarte.
            </h2>
            <br></br>
            <br></br>
            <p>
            Somos una familia que rooeda en comunidad hacia el 
            bienestar físico y emocional.
            Somos un studio de indoor cycling en donde roodamos al ritmo de la música.
            Nos apasiona rodar y sentirnos vivos, somos esa sensación de adrenalina que siempre 
            quieres volver a experimentar. Por eso estamos aquí.
            </p>
            <br></br>
            <br></br>
            {renderAuthButton()}
          </div>
          <div className="bx--col-sm-8 bx--col-lg-6"></div>
          
          </div>

          <div className="bx--row">
          <div className="bx--col-md-12 bx--col-lg-12">
                <ul className="socialNetworks" data-type='heroHome'>
                  <li>
                    <Link element={Link} to="/">
                    <img src={IconInstagram} alt='website logo' />  
                    </Link>
                  </li>
                  <li>
                    <Link element={Link} to="/">
                    <img src={IconFacebook} alt='website logo' />  
                    </Link>
                  </li>
                </ul>
          </div>          
          </div>

      </div>
    </div>

    <Footer />
    </>
  );
}
};
export default Studio;
