import React from 'react';
import './Home.scss?v=2.5.0';
import IconRooeda from './icon-rooeda.svg';
import IconFacebook from './icon-facebook.svg';
import IconInstagram from './icon-instagram.svg';
import ImgRooeda from './instructors-bg.png';
import { Link } from 'react-router-dom';
import { Button } from 'carbon-components-react';
import { ChevronRight32, Cyclist32, Launch16 } from '@carbon/icons-react';
import Footer from '../../components/Footer';
import Packs from '../../components/Packs';
import ls from 'local-storage'


const renderAuthButton = ()=>{
  let session = ls.get('session');
  if(session == null){
    return  <Link element={Link} to="/signup">
              <Button renderIcon={Cyclist32}>Regístrate ahora</Button>
            </Link>
  } else{
    return <Link element={Link} to="/packs">
              <Button renderIcon={Cyclist32}>Comprar pack</Button>
           </Link>
  }
}

const Home = () => {
  return (
    <>
    <div className="heroHome">
      <div className="bx--grid bx--grid--full-width landing-page">
        <div className="heroHeadline">

          <div className="alignBottom" data-type='home'>
            <h1 >Somos un indoor cycling studio.</h1>
            <br></br>
            {renderAuthButton()}

            <ul className="socialNetworks" data-type='heroHome'>
                  <li>
                    <a href="https://www.instagram.com/rooedastudio/">
                      <img src={IconInstagram} alt='Rooeda™ Studio' />  
                    </a>
                  </li>
                  <li>
                    <a href="https://facebook.com">
                      <img src={IconFacebook} alt='Rooeda™ Studio' />  
                    </a>
                  </li>
                </ul>
          </div>

        </div>

          <div className="bx--row">

          <div className="bx--col-md-12 bx--col-lg-12">

          </div>
          </div>
    </div>

    </div>
    
    <div className="aboutHome">
    <div className="bx--grid">
    <div className="bx--row">
        <div className="bx--col-md-8">
            <img src={IconRooeda} alt='website logo' className="footerIconRooeda" />
            <h1>Roodamos y nos hacemos más fuertes.</h1>
            <br></br>
            <br></br>
            <p>
            <strong>Rooeda™</strong> es una experiencia, buscamos inspirarte, ponerte retos y llevarte hacia adelante.  
            <strong> Somos una familia</strong> que <strong>rooeda</strong> en comunidad hacia el bienestar físico y emocional.
            A retarte a ser mejor, no sólo en la bici sino en tu vida.  
            <span style={{fontStyle: "oblique"}}> Queremos ser la mejor parte de tus días. 
            </span>
            </p>
            <br></br>
            <br></br>
            <Link element={Link} to="/studio">
              <Button renderIcon={ChevronRight32}>Conoce el studio</Button>
            </Link>
            <br></br>
            <a href="https://open.spotify.com/playlist/4kf66oX0IqZbugIHADK6iV?si=UmHlctbOSc6CvXaUnNfUOQ" className="spotifyButton" target="_blank" rel="noopener noreferrer">
              Playlist mensual en Spotify
              <Launch16 className="spotifyIcon"/>
            </a>
          </div>
          <div className="bx--col-md-8">
            <img src={ImgRooeda} alt='website logo' className="imgRooeda" />
          </div>

    </div>
    </div>
    </div>
    <Packs />
    <Footer />
    </>
  );
};
export default Home;
