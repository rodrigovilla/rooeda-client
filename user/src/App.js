import React, { Component } from 'react';
import './App.scss';
import { Route, Switch } from 'react-router-dom';
import { Content } from 'carbon-components-react/lib/components/UIShell';
import Header from './components/Header';
import Calendar from './components/Calendar';
import Sidebar from './components/Sidebar';
import Packs from './components/Packs';
import PickupBike from './components/PickupBike';
import CreditCard from './components/CreditCard';
import Home from './content/Home';
import Studio from './content/Studio';
import Instructors from './content/Instructors';
import Profile from './content/Profile';
import Signup from './content/Signup';
import Login from './content/Login';
import ForgotPass from './content/ForgotPass';
import NewPass from './content/NewPass';
import Redirect from './content/Redirect';
import Logout from './content/Logout';
import MercadoPago from './components/MercadoPago';
import PrivacyPolicy from './content/PrivacyPolicy';
import TermsAndConditions from './content/TermsAndConditions';
import CacheBuster from './CacheBuster';
import { ToastContainer } from 'react-toastify';

class App extends Component {
  render() {
    return (
      <>
        <ToastContainer 
            position="top-right"
            autoClose={3000}
        >
        </ToastContainer>

        <Content>
        <Sidebar></Sidebar>
        <Header></Header>
        <CacheBuster>
        {({ loading, isLatestVersion, refreshCacheAndReload }) => {
          if (loading) return null;
          if (!loading && !isLatestVersion) {
            refreshCacheAndReload();
          }
          
          return null;
        }}
      </CacheBuster>
          <Switch>
            <Route exact path="/" component={Home} />
            <Route path="/studio" component={Studio} />
            <Route path="/instructors" component={Instructors} />
            <Route path="/packs" component={Packs} />
            <Route path="/profile" component={Profile} />
            <Route path="/signup" component={Signup} />
            <Route path="/login" component={Login} />
            <Route path="/forgotpass" component={ForgotPass} />
            <Route path="/newpass" component={NewPass} />
            <Route path="/pickupbike" component={PickupBike} />
            <Route path="/calendar" component={Calendar} />
            <Route path="/redirect" component={Redirect} />
            <Route path="/logout" component={Logout} />
            <Route path="/mercado-pago" component={MercadoPago} />
            <Route path="/creditcard" component={CreditCard} />
            <Route path="/privacy-policy" component={PrivacyPolicy} />
            <Route path="/terms-and-conditions" component={TermsAndConditions} />
          </Switch>
        </Content>

      </>
    );
  }
}

export default App;
