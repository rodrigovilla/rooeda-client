
import CeditCard from './credit-card.png';
import './CreditCard.scss';
import { Form, Button, TextInput } from 'carbon-components-react';
import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import packType from '../../helpers/pack-type';
import ls from 'local-storage'
import axios from 'axios';
import appSettings from '../../helpers/AppSettings';
import {
  StripeProvider,
  Elements,
  CardElement,
  injectStripe
} from 'react-stripe-elements';
import NumberFormat from 'react-number-format';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const CARD_OPTIONS = {
  iconStyle: 'solid',
  style: {
    base: {
      iconColor: '#c4f0ff',
      color: '#fff',
      fontWeight: 500,
      fontFamily: 'Roboto, Open Sans, Segoe UI, sans-serif',
      fontSize: '1rem',
      fontSmoothing: 'antialiased',
      ':-webkit-autofill': {color: '#fce883'},
      '::placeholder': {color: '#87bbfd'},
    },
    invalid: {
      iconColor: '#ff0000',
      color: '#ffc7ee',
    },
  },
};

const EmailInputProps = {
  className: 'age',
  id: 'email',
  labelText: 'Correo electrónico',
  placeholder: ''
};

class _CardForm extends React.Component {
  constructor(props) {
    super(props)

    const session = ls.get('session');

    if(!session){
      window.location.href='/login'
      return;
    }

    let pack = ls.get('packType');

    if(pack === null){
      window.location.href='/packs'
      return;
    }

    this.state = {
      price: packType[pack].Price,
      email: '',
      hasEmail: true,
      buttonDisabled: false
    };
    
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
  };

  notify = () => toast("Pago procesado correctamente. Gracias!");

  componentDidMount() {
    axios.get(`${appSettings.SERVER_URL}/users/id/${ls.get('session')}`).then(customer => {
      if(customer.data.Email){
        this.setState({hasEmail: true});
      }else{
        this.setState({hasEmail: false});
      }
    });
  }

  handleChange(event) {
    let nam = event.target.name;
    let val = event.target.value;
    this.setState({[nam]: val});
  }

  handleSubmit(e){
    e.preventDefault();
    this.props.stripe.createToken().then(payload => {
      const sessionId = ls.get('session');
      const amount = this.state.price;
      
      if(!payload.token){
        toast("Datos invalidos");
        return;
      }

      this.setState({ buttonDisabled: true });
      let data = { Session: sessionId, Token: payload.token, Email: this.state.email };
      axios.post(`${appSettings.SERVER_URL}/payments/user`, data).then(customer => {
        let user = {
          StripeId: customer.data.StripeId,
          Amount: amount,
          Name: customer.data.Name
        }

        axios.post(`${appSettings.SERVER_URL}/payments/checkout`, user).then( payment => {
          if(payment.data.success){
            this.notify();

            let createClass = {
              UserId: sessionId,
              ClassId: ls.get('packType')
            };

            axios.post(`${appSettings.SERVER_URL}/user-pack`, createClass).then( userPack => {
              if(userPack.status === 200){
                ls.remove('packType');
                setTimeout(() => {
                window.location.href='/profile'
                }, 4000);
              }else{
                toast("Error al añadir el pack.");
              }
            });
          }else{
            toast("Error al procesar el pago.");
            this.setState({ buttonDisabled: false });
          }
        });
      });
    });
  }

  render() {
    return (
      <div className="standardContainer">
        <div className="bx--grid">
          <div className="bx--row">
            <div className="bx--col-lg-4 bx--col-md-2 bx--col-sm-1"></div>
              <div className="bx--col-lg-4 bx--col-md-4 bx--col-sm-4">
                <Form onSubmit={this.handleSubmit}>
                    <h3 className="formTitles"> Compra tu pack </h3>
                    <img src={CeditCard} alt='Rooeda Credit Card' className="creditCard" />
                    <br></br>
                    <br></br>
                    {(!this.state.hasEmail) ? 
                    <TextInput
                    name='email'
                    type="email" 
                    required
                    value={this.state.value} 
                    onChange={this.handleChange} 
                    
                    {...EmailInputProps}  
                    />
                    : null } 
                    <CardElement required options={CARD_OPTIONS} className="cardElement"/>
                    <Button type="submit" value="Submit" className="buttonAccess" disabled={this.state.buttonDisabled}>
                      Pagar <NumberFormat value={this.state.price} displayType={'text'} thousandSeparator={true} prefix={'$'} />
                    </Button>
                </Form>
                <div className="alignBottom" data-type='privacyPolicy'>
                <div className="privacyPolicy">Al continuar, aceptas las
                    <Link element={Link} to="/login" className="privacyPolicy">
                      Condiciones de uso
                    </Link>
                      y el 
                    <Link element={Link} to="/login" className="privacyPolicy">
                      Aviso de privacidad
                    </Link>
                      de Rooeda™ Studio.
                </div>
              </div>
            </div>  
            <div className="bx--col-lg-4 bx--col-md-2 bx--col-sm-1"></div>
          </div>
        </div>
      </div>
    ) 
  }
}

const CardForm = injectStripe(_CardForm)

class Checkout extends React.Component {
  render() {
    return  (
      <Elements locale='es'>
        <CardForm />
      </Elements> 
    )
  }
}

const CreditCard = () => {
  const [stripeLoaded, setStripeLoaded] = useState({})
  const  loadScript = src =>
  new Promise((resolve, reject) => {
    const script = document.createElement('script')
    script.src = src
    script.addEventListener('load', () => {
      resolve({ successful: true })
    })
    script.addEventListener('error', event => {
      reject({ error: event })
    })
    document.head.appendChild(script)
  })
  
  useEffect(() => {
    const fetchData = async () => {
      const result = await loadScript(appSettings.STRIPE_SCRIPT_URL);
      setStripeLoaded(result);
    }
    fetchData()
  }, [])

  return  stripeLoaded.successful ? (
    <StripeProvider apiKey={process.env.REACT_APP_STRIPE_PUBLIC_KEY}>
      <Checkout />
    </StripeProvider>
  ) : null
}

export default CreditCard;