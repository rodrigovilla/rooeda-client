import React from 'react';
import { Link } from 'react-router-dom';

const Agreement = () => (

    <div className="alignBottom" data-type='privacyPolicy'>
    <div className="privacyPolicy">Al continuar, aceptas las 
        <Link element={Link} to="/terms-and-conditions" className="privacyPolicy">
          Condiciones de uso
        </Link>
          y el
        <Link element={Link} to="/privacy-policy" className="privacyPolicy">
          Aviso de privacidad
        </Link>
          de Rooeda™ Studio.
    </div>
    </div>

);

export default Agreement;