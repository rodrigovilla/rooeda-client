import React from 'react';
import './PickupBike.scss';
import { Button } from 'carbon-components-react';
import { Cyclist32 } from '@carbon/icons-react';
import Agreement from '../../components/Agreement';
import ls from 'local-storage'
import axios from 'axios';
import 'react-responsive-modal/styles.css';
import { Modal } from 'react-responsive-modal';
import appSettings from '../../helpers/AppSettings';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

class pickUpBike extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        data: [],
        selected: '',
        schedule: {},
        open: false,
        buttonDisabled: false
      };

      this.handleSubmit = this.handleSubmit.bind(this);
    }

    notify = () => toast("Bicicleta confirmada. ¡A roodar!");

    componentDidMount() {
      const session = ls.get('session');

      if(!session){
        window.location.href='/login'
        return;
      }

      var schedule = ls.get('schedule');

      if(!schedule){
        window.location.href='/calendar'
        return;
      }

      axios.post(`${appSettings.SERVER_URL}/bikes/available`, schedule)
      .then( bikes  => {
        bikes.data.forEach(x => x.Disabled = x.Available );
        this.setState({
          data: bikes.data,
          sData: bikes.data,
          schedule: ls.get('schedule')
        });
      })
      .catch(error => this.setState({ error, isLoading: false }));
    }

    takeBike(id){
      var list =  this.state.data;
      list.forEach(x =>  x.Available = true);
      list[parseInt(id - 1)].Available = !list[parseInt(id - 1)].Available;
      this.setState({data: list, selected: list[parseInt(id - 1)].Id}); 
    }

    handleSubmit(){
      let json = {
        Bike: this.state.selected,
        Schedule: ls.get('schedule'),
        Session: ls.get('session')
      };

      if(!json.Bike){
        this.setState({ open: true });
        return;
      }

      this.setState({buttonDisabled: true});

      axios.get(`${appSettings.SERVER_URL}/user-pack/available-to-buy/${json.Session}`).then( result => {
        var totalClases = 0;
        
        if(result.data.length > 0){
          result.data.forEach(element => {
            totalClases += element.Clases;
          });
        }

        if(totalClases > 0){
                axios.put(`${appSettings.SERVER_URL}/user-pack/delete-class/${result.data[0].PackId}`)
                .then(userPack => {
                    if(userPack.status === 200){
                      axios.post(`${appSettings.SERVER_URL}/user-schedule`, json)
                      .then(userSchedule => {
                          if(userSchedule.status === 200){
                            if(userSchedule.data.status === 200){
                              this.notify()
                              this.setState({buttonDisabled: false});
                              return;
                            }
                            
                            toast(userSchedule.data.message);
                            setTimeout(() => {
                              window.location.href='/profile'
                              }, 4000);
                          }else{
                            toast(userSchedule.data.message);
                            this.setState({buttonDisabled: false});
                          }
                      }).catch(error => {
                          //--- aqui un toast de error, debido a que la respuesta fue invalida
                        toast('Error');
                        this.setState({buttonDisabled: false});
                      });
                    }else{
                      //--- aqui un toast de error, debido a que la respuesta fue invalida
                      toast('Error');
                      this.setState({buttonDisabled: false});
                    }
                  }).catch(error => {
                    //--- aqui un toast de error, debido a que la respuesta fue invalida
                    toast('Error');
                    this.setState({buttonDisabled: false});
                });
        }else{
          window.location.href='/packs'
          return;
        }
      }); 
    }

    onOpenModal = () => {
      this.setState({ open: true });
    };
  
    onCloseModal = () => {
      this.setState({ open: false })
    };

    render() {
      const { data, open } = this.state;
      
      return (
        <>
          <Modal 
            open={open} 
            onClose={this.onCloseModal} 
            center
            animationDuration={80}
          >
          <div>
          <p>
            <strong>¡Por favor selecciona tu bici!</strong>
          </p>
             <br></br>
             <Button 
                renderIcon={Cyclist32 }  
                type="submit" 
                value="Submit" 
                size='small'
                kind='ghost' 
                onClick={this.onCloseModal}
              >
                A roodar
            </Button>
          </div>
         </Modal>
        
        <div className="standardContainer">
          <h1>Bicicleta</h1>
          <br></br>
          <br></br>
          <div className="studioContainer">
            <div className="studio">
              <div className="studioRow">
                {(data.map((bike, index) =>  {
                  return (bike.Id <= 8) ?
                  (
                    (bike.Disabled) ?
                    <div key={bike.Id}>
                      <div className="section">
                      <div className="bike" >
                        <input type="checkbox" value="None" id={bike.Id} name="check" checked={!bike.Available} onClick={() => this.takeBike(bike.Id)}/>
                        <label for={bike.Id} >
                        <p>{bike.Id}</p>
                        </label> 
                      </div>
                    </div>
                    </div>
                    : 
                    <div key={bike.Id}>
                    <div className="section">
                    <div className="bike" >
                      <input type="checkbox" value="None" id={bike.Id} name="check" disabled={true} checked={false}/>
                      <label for={bike.Id}  className="tr">
                      <p>{bike.Id}</p>
                      </label> 
                    </div>
                  </div>
                  </div>
                  )
                  : null
                }))}
              </div>

              <div className="studioRow">
                {(data.map((bike, index) =>  {
                  return (bike.Id > 8 && bike.Id <= 16) ?
                  (bike.Disabled) ?
                    <div key={bike.Id}>
                      <div className="section">
                      <div className="bike" >
                        <input type="checkbox" value="None" id={bike.Id} name="check" checked={!bike.Available} onClick={() => this.takeBike(bike.Id)}/>
                        <label for={bike.Id} >
                        <p>{bike.Id}</p>
                        </label> 
                      </div>
                    </div>
                    </div>
                    : 
                    <div key={bike.Id}>
                    <div className="section">
                    <div className="bike" >
                      <input type="checkbox" value="None" id={bike.Id} name="check" disabled={true} checked={false}/>
                      <label for={bike.Id} className="tr">
                      <p>{bike.Id}</p>
                      </label> 
                    </div>
                  </div>
                  </div>
                  : null
                }))}
              </div>
            </div>
          </div> 
            
          <div className="studioContainer" data-type='paddingBottom'>
            <div className="studio">
              <div className="studioRow">
                {(data.map((bike, index) =>  {
                  return (bike.Id > 16 && bike.Id <= 24) ?
                  (
                    (bike.Disabled) ?
                    <div key={bike.Id}>
                      <div className="section">
                      <div className="bike" >
                        <input type="checkbox" value="None" id={bike.Id} name="check" checked={!bike.Available} onClick={() => this.takeBike(bike.Id)}/>
                        <label for={bike.Id} >
                        <p>{bike.Id}</p>
                        </label> 
                      </div>
                    </div>
                    </div>
                    : 
                    <div key={bike.Id}>
                    <div className="section">
                    <div className="bike" >
                      <input type="checkbox" value="None" id={bike.Id} name="check" disabled={true} checked={false}/>
                      <label for={bike.Id}  className="tr">
                      <p>{bike.Id}</p>
                      </label> 
                    </div>
                  </div>
                  </div>
                  )
                  : null
                }))}
              </div>

              <div className="studioRow">
                {(data.map((bike, index) =>  {
                  return (bike.Id > 24) ?
                  (
                    (bike.Disabled) ?
                    <div key={bike.Id}>
                      <div className="section">
                      <div className="bike" >
                        <input type="checkbox" value="None" id={bike.Id} name="check" checked={!bike.Available} onClick={() => this.takeBike(bike.Id)}/>
                        <label for={bike.Id} >
                        <p>{bike.Id}</p>
                        </label> 
                      </div>
                    </div>
                    </div>
                    : 
                    <div key={bike.Id}>
                    <div className="section">
                    <div className="bike" >
                      <input type="checkbox" value="None" id={bike.Id} name="check" disabled={true} checked={false}/>
                      <label for={bike.Id}  className="tr">
                      <p>{bike.Id}</p>
                      </label> 
                    </div>
                  </div>
                  </div>
                  )
                  : null
                }))}
              </div>
            </div>
          </div>
        <Button renderIcon={Cyclist32} onClick={this.handleSubmit} disabled={this.state.buttonDisabled}> 
          Confirmar
        </Button> 

        <Agreement></Agreement>

        </div>
      </>
      );
    } 
  }
  export default pickUpBike;