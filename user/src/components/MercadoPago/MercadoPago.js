
import CeditCard from './credit-card.png';
import './MercadoPago.scss';
import { Button, TextInput } from 'carbon-components-react';
import React from 'react';
import { Link } from 'react-router-dom';
import packType from '../../helpers/pack-type';
import ls from 'local-storage'
import axios from 'axios';
import appSettings from '../../helpers/AppSettings';
import NumberFormat from 'react-number-format';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

class MercadoPago extends React.Component {
  constructor(props) {
    super(props)

    const session = ls.get('session');

    if(!session){
      window.location.href='/login'
      return;
    }

    let pack = ls.get('packType');

    if(pack === null){
      window.location.href='/packs'
      return;
    }

    this.state = {
      price: packType[pack].Price,
      email: '',
      buttonDisabled: false,
      issuer: '',
      user: []
    };

    this.handleChange = this.handleChange.bind(this);
  };

  notify = () => toast("Pago procesado correctamente. Gracias!");

  componentDidMount() {
    this.loadMercadoPago();
  
    axios.get(`${appSettings.SERVER_URL}/users/id/${ls.get('session')}`).then(customer => {
      if(customer.data){
        this.setState({user: customer.data});
      }
    });
  }

  handleChange(event) {
    let nam = event.target.name;
    let val = event.target.value;
    this.setState({[nam]: val});
  }

  async loadMercadoPago() {
    const loadScript = (url, callback ) => {
      let script = document.createElement('script');
      script.type = 'text/javascript';
  
      if (script.readyState) {
        script.onreadystatechange = () => {
          if (
            script.readyState === 'loaded' ||
            script.readyState === 'complete'
          ) {
            script.onreadystatechange = null;
            callback();
          }
        };
      } else {
        script.onload = () => callback();
      }
      
      script.src = url;
      document.getElementsByTagName('head')[0].appendChild(script);
    };

    const handleScriptLoad = async () => {
      const mp = new window.MercadoPago(process.env.REACT_APP_MERCADO_PAGO_PUBLIC_KEY, { locale: 'es-MX' });

      const cardForm = mp.cardForm({
        amount: `${this.state.price}`,
        autoMount: true,
        processingMode: 'aggregator',
        form: {
            id: 'form-checkout',
            cardholderName: {
                id: 'form-checkout__cardholderName',
                placeholder: 'Nombre',
            },
            cardholderEmail: {
                id: 'form-checkout__cardholderEmail',
                placeholder: 'Email',
            },
            cardNumber: {
                id: 'form-checkout__cardNumber',
                placeholder: 'Numero de tarjeta',
            },
             cardExpirationMonth: {
                id: 'form-checkout__cardExpirationMonth',
                placeholder: 'MM'
            },
            cardExpirationYear: {
                id: 'form-checkout__cardExpirationYear',
                placeholder: 'YYYY'
            },
            securityCode: {
                id: 'form-checkout__securityCode',
                placeholder: 'CVV',
            },
            installments: {
                id: 'form-checkout__installments',
                placeholder: 'Total installments'
            },
            identificationType: {
                id: 'form-checkout__identificationType',
                placeholder: 'Document type'
            },
            identificationNumber: {
                id: 'form-checkout__identificationNumber',
                placeholder: 'Numero de documeto'
            },
            issuer: {
                id: 'form-checkout__issuer',
                placeholder: 'Issuer'
            }
        },
        callbacks: {
           onFormMounted: error => {
               if (error) return console.warn('Form Mounted handling error: ', error)
               //console.log('Form mounted')
           },
           onFormUnmounted: error => {
               if (error) return console.warn('Form Unmounted handling error: ', error)
               //console.log('Form unmounted')
           },
           onIdentificationTypesReceived: (error, identificationTypes) => {
               if (error) return console.warn('identificationTypes handling error: ', error)
               //console.log('Identification types available: ', identificationTypes)
           },
           onPaymentMethodsReceived: (error, paymentMethods) => {
               if (error) return console.warn('paymentMethods handling error: ', error)
               //console.log('Payment Methods available: ', paymentMethods)
               this.setState({issuer:paymentMethods[0].issuer.name})
           },
           onIssuersReceived: (error, issuers) => {
               if (error) return console.warn('issuers handling error: ', error)
               //console.log('Issuers available: ', issuers)
           },
           onInstallmentsReceived: (error, installments) => {
               if (error) return console.warn('installments handling error: ', error)
               //console.log('Installments available: ', installments)
           },
           onCardTokenReceived: (error, token) => {
               if (error) return console.warn('Token handling error: ', error)
               //console.log('Token available: ', token)
           },
           onSubmit: async (event) => {
              event.preventDefault();

              const {
                paymentMethodId,
                issuerId,
                cardholderEmail: email,
                amount,
                token,
                installments,
                identificationNumber,
                identificationType,
              } = cardForm.getCardFormData();

              if(!token){
                toast("Datos invalidos");
                return;
              }

              this.setState({ buttonDisabled: true });

              let requestBody = {
                token,
                issuerId,
                paymentMethodId,
                transaction_amount: amount,
                installments: installments,
                description: `Se pago un paquete de $ ${this.state.price}`,
                payer: {
                  email,
                  identification: {
                    type: identificationType,
                    number: identificationNumber,
                  },
                },
              }

              let checkout = await axios.post(`${appSettings.SERVER_URL}/payments/checkout-mp`, requestBody)

              if((checkout.data || checkout.data.status) && checkout.data.status === 'approved'){
                this.notify();

                let createClass = {
                  UserId: ls.get('session'),
                  ClassId: ls.get('packType')
                };

                let userPack  = await axios.post(`${appSettings.SERVER_URL}/user-pack`, createClass);
                  if(userPack.status === 200){
                    ls.remove('packType');
                    setTimeout(() => {
                    window.location.href='/profile'
                    }, 4000);
                  }else{
                    toast("Error al añadir el pack.");
                  }
              }else{
                toast("Error al procesar el pago.");
                this.setState({ buttonDisabled: false });
              }
           },
           onFetching:(resource) => {
               console.log('Fetching resource: ', resource)
               console.log( resource)
  
               // Animate progress bar
               const progressBar = document.querySelector('.progress-bar')
               progressBar.removeAttribute('value')
  
               return () => {
                   progressBar.setAttribute('value', '0')
               }
           },
        }
      })

    };
  
    loadScript(appSettings.MERCADO_PAGO_SCRIPT_URL, handleScriptLoad);
  }

  render() {
    return (
      <div className="standardContainer">
        <div className="bx--grid">
          <div className="bx--row">
            <div className="bx--col-lg-4 bx--col-md-2 bx--col-sm-1"></div>
              <div className="bx--col-lg-4 bx--col-md-4 bx--col-sm-4">
              <h3 className="formTitles"> Compra tu pack </h3>
                    <img src={CeditCard} alt='Rooeda Credit Card' className="creditCard" />
                    <br/>   <br/>
              <h4>{this.state.issuer}</h4> 
              <form id="form-checkout" >
                <TextInput type="text" className="cardNumber" id="form-checkout__cardNumber"/>
                <TextInput type="text" className="cardExpirationMonth" id="form-checkout__cardExpirationMonth"/>
                <TextInput type="text" className="cardExpirationYear" id="form-checkout__cardExpirationYear" />
                <TextInput type="text" className="cardholderName" id="form-checkout__cardholderName"/>
                {(this.state.user || this.state.user.Email) ?
                  <TextInput type="email" className="cardholderEmail" id="form-checkout__cardholderEmail" value={this.state.user.Email || ''} hidden/>
                : 
                  <TextInput type="email" className="cardholderEmail" id="form-checkout__cardholderEmail" />
                }
                <TextInput type="text" className="securityCode" id="form-checkout__securityCode" />
                <select className="issuer" id="form-checkout__issuer" hidden></select>
                <select className="identificationType" id="form-checkout__identificationType" hidden></select>
                <TextInput type="text" className="identificationNumber" id="form-checkout__identificationNumber" value="123581321" hidden/>
                <select className="installments" id="form-checkout__installments" hidden></select>
                <Button type="submit" id="form-checkout__submit" disabled={this.state.buttonDisabled}>Pagar &nbsp;<NumberFormat value={this.state.price} displayType={'text'} thousandSeparator={true} prefix={'$'} /></Button>
                <progress value="0" className="progress-bar" hidden>Cargando...</progress>
            </form>
              <div className="alignBottom" data-type='privacyPolicy'>
              <div className="privacyPolicy">Al continuar, aceptas las
                  <Link element={Link} to="/login" className="privacyPolicy">
                    Condiciones de uso
                  </Link>
                    y el 
                  <Link element={Link} to="/login" className="privacyPolicy">
                    Aviso de privacidad
                  </Link>
                    de Rooeda™ Studio.
              </div>

              </div>
            </div>  
            <div className="bx--col-lg-4 bx--col-md-2 bx--col-sm-1"></div>
          </div>
        </div>
      </div>
    ) 
  }
}

export default MercadoPago;