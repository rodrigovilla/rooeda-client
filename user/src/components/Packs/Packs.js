import React from 'react';
import './Packs.scss';
import ls from 'local-storage'
import axios from 'axios';
import appSettings from '../../helpers/AppSettings';
import { toast } from 'react-toastify';

class Packs extends React.Component {
  
  handleSubmit(e){
    const session = ls.get('session');

    if(!session){
      window.location.href='/login'
      return;
    }

    ls.set('packType', e);

    if (e !== 0){
      window.location.href='/creditcard'
      return;
    }

    axios.get(`${appSettings.SERVER_URL}/user-pack/pack-zero/${session}`)
      .then( packs  => {
        if(packs.data.Found){
          toast('Se termino el limite de clases de prueba');
        }else{
          window.location.href='/creditcard'
        }
      }).catch(error => this.setState({ error }));
  }

  render() {
    return (
      <>
      <div className="bx--grid bx--grid--full-width landing-page pickupPackContainer">
      <div className="bx--row">
        <div className="bx--col">
          <div className="packTitle">
            <h1>Elige tu pack</h1>
          </div>
        </div>
      </div>
      
      <div className="pickupPack">
      {/* Row starts */}
      <div className="bx--row">
          <div className="bx--col-md-4 bx--col-lg-2"></div>
          <div className="bx--col-md-4 bx--col-lg-2"></div>
          <div className="bx--col-md-4 bx--col-lg-2">
              <div className="bx--aspect-ratio bx--aspect-ratio--1x1 packBox" >
              <div className="bx--aspect-ratio--object" onClick={() => this.handleSubmit(0)}>
                <div className="packContent" >
                  <h2>1er</h2>
                  <h4>Clase</h4>
                <div className="packContentBottom">
                  <p>$90</p>
                  <span>Expira en 1 día</span>
                </div>
                </div>
              </div>  
              </div> 
          </div>
          <div className="bx--col-md-4 bx--col-lg-2">
              <div className="bx--aspect-ratio bx--aspect-ratio--1x1 packBox" >
              <div className="bx--aspect-ratio--object">
                <div className="packContent" onClick={() => this.handleSubmit(1)}>
                  <h2>1</h2>
                  <h4>Clase</h4>
                <div className="packContentBottom">
                  <p>$120</p>
                  <span>Expira en 5 días</span>
                </div>
                </div>
              </div>  
              </div> 
          </div>
          <div className="bx--col-md-4 bx--col-lg-2"></div>
          <div className="bx--col-md-4 bx--col-lg-2"></div>
      </div>

      {/* Row starts */}
      <div className="bx--row">
          <div className="bx--col-md-4 bx--col-lg-2"></div>
          <div className="bx--col-md-4 bx--col-lg-2"></div>
          <div className="bx--col-md-4 bx--col-lg-2">
              <div className="bx--aspect-ratio bx--aspect-ratio--1x1 packBox" >
              <div className="bx--aspect-ratio--object">
                <div className="packContent" onClick={() => this.handleSubmit(2)}>
                  <h2>5</h2>
                  <h4>Clases</h4>
                <div className="packContentBottom">
                  <p>$550</p>
                  <span>Expira en 15 días</span>
                </div>
                </div>
              </div>  
              </div> 
          </div>
          <div className="bx--col-md-4 bx--col-lg-2">
              <div className="bx--aspect-ratio bx--aspect-ratio--1x1 packBox" >
              <div className="bx--aspect-ratio--object">
                <div className="packContent" onClick={() => this.handleSubmit(3)}>
                  <h2>10</h2>
                  <h4>Clases</h4>
                <div className="packContentBottom">
                  <p>$1,000</p>
                  <span>Expira en 30 días</span>
                </div>
                </div>
              </div>  
              </div> 
          </div>
          <div className="bx--col-md-4 bx--col-lg-2"></div>
          <div className="bx--col-md-4 bx--col-lg-2"></div>
      </div>

      {/* Row starts */}
      <div className="bx--row">
          <div className="bx--col-md-4 bx--col-lg-2"></div>
          <div className="bx--col-md-4 bx--col-lg-2"></div>
          <div className="bx--col-md-4 bx--col-lg-2">
              <div className="bx--aspect-ratio bx--aspect-ratio--1x1 packBox" >
              <div className="bx--aspect-ratio--object">
                <div className="packContent" onClick={() => this.handleSubmit(4)}>
                  <h2>15</h2>
                  <h4>Clases</h4>
                <div className="packContentBottom">
                  <p>$1,350</p>
                  <span>Expira en 40 días</span>
                </div>
                </div>
              </div>  
              </div> 
          </div>
          <div className="bx--col-md-4 bx--col-lg-2">
              <div className="bx--aspect-ratio bx--aspect-ratio--1x1 packBox" >
              <div className="bx--aspect-ratio--object">
                <div className="packContent" onClick={() => this.handleSubmit(5)}>
                  <h2>25</h2>
                  <h4>Clases</h4>
                <div className="packContentBottom">
                  <p>$2,050</p>
                  <span>Expira en 60 días</span>
                </div>
                </div>
              </div>  
              </div> 
          </div>
          <div className="bx--col-md-4 bx--col-lg-2"></div>
          <div className="bx--col-md-4 bx--col-lg-2"></div>
      </div>

      {/* Row starts */}
      <div className="bx--row">
          <div className="bx--col-md-4 bx--col-lg-2"></div>
          <div className="bx--col-md-4 bx--col-lg-2"></div>
          <div className="bx--col-md-4 bx--col-lg-2">
              <div className="bx--aspect-ratio bx--aspect-ratio--1x1 packBox" >
              <div className="bx--aspect-ratio--object">
                <div className="packContent" onClick={() => this.handleSubmit(6)}>
                  <h2>50</h2>
                  <h4>Clases</h4>
                <div className="packContentBottom">
                  <p>$3,750</p>
                  <span>Expira en 105 días</span>
                </div>
                </div>
              </div>  
              </div> 
          </div>
          <div className="bx--col-md-4 bx--col-lg-2">
              <div className="bx--aspect-ratio bx--aspect-ratio--1x1">
              <div className="bx--aspect-ratio--object">
              </div>  
              </div> 
          </div>
          <div className="bx--col-md-4 bx--col-lg-2"></div>
          <div className="bx--col-md-4 bx--col-lg-2"></div>
      </div>
      </div>


      {/* Grid ends */}
      </div>
      </>
    );
  } 
}
export default Packs;
